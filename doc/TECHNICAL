*** Technical information ***

** Notes **

All color information is stored as RGB with 32bit float channels
All directional vectors are in world coordinates and normalized

** Data types **

All internal data types are defined in types.h, included by both
clTracer binaries and computation kernels - binaries must be
recompiled when any datatype is altered!

Data type definitions are using __OPENCL_VERSION__ macro to determine
which part is using them, and use correct types (such as cl_mem for
OpenCL pointers or cl_floatx for OpenCL floatx vectors)

All data structures are named 'name_s', and typedef'd to name_t for
programmers convenience. All constants have to be defined as macros

** Shaders **

all shaders consist of two parts, one for preparing space for reflected
and/or refracted rays (referred to as "counting"), the other for
computing the pixel color and direction of new rays (referred to as
"drawing").

formal declaration of the counting part is:
int shadername_count(__private const struct hit_s * hit,
	__global const struct triangle_s * triangle);

first argument (called "hit" in this example) points to structure
representing the actual intersection point, by index of triangle
(not necessary for this function), barycentric coordinates, the 
direction of the ray and image reconstruction information (also
not necessary here).

second argument points to triangle which intersected the ray, which
can be used to compute world, texture and normal coordinates of the
intersection point.

a non-negative number should be returned, representing the number of
reflected/refracted rays shooting from this point


formal declaration of the drawing part is:
void shadername(__private const struct hit_s * hit,
	__global const struct triangle_s * triangle,
	__global struct ray_s * rays,
	__global struct pixel_s * image,
        __global const struct light_sample_s * light_samples,
	__global const struct light_s * lights,
	__const int lightcount,
	__read_only image2d_t tex)

first and second argument are the same as for the counting part.
the image reconstruction part is mostly used when manipulating the
output image (4th argument), and will be explained there

third argument points to space allocated for new rays - it is needed
to create exactly that many rays as the value returned by counting
part, however it is possible to create rays with no zero importance
somewhere outside the scene. new rays have to have a valid origin
(probably the current intersection point), a direction, and the image
reconstruction information (see 4th argument)

fourth argument points to the output image as a one-dimensional array
of pixels, each pixel containing color, fill ratio (sum of importances
of all rays already affecting the color, should be between one and zero
at all times), and a locking handle (locking support is considered
highly experimental at this stage). importance of the current ray and
index of pixel represented by this ray forms the image reconstruction
part of first argument

fifth, sixth and seventh argument are used for lighting, being actual
light sample values for current intersection point, list of all light
sources in the scene, and the number of those lights, in that order.
light sample contains light color, intensity (blocked light has zero
intensity, otherwise it decreases with distance squared) and direction
vector from the light. the light sources containt light type (ambient,
directional, omni or spot) and information already present in light
sample. the number of samples is one per lightsource and ray, samples
and lights are stored in the same order

last several arguments (in this example one, the actual number is
specified in material file referenced from the scene file) are textures
associated with this material, as loaded by the control program

* Special shaders *

one special shader exists in clTracer, the background shader. this
shader is used on rays not intersecting with the scene, and on rays
which are considered dead. the shader is specified in mat/background.cl,
by two functions, "islive" and "background", with following declarations;

int islive (__private struct reconstruct_s * ray)

void background (__private struct hit_s * hit,
	__global struct pixel_s * image)

islive should return 1 for live rays and 0 for dead rays, probably by
comparing ray importance with some constant (you are free to use any
method you want).

background should work the same as other shader, using hit structure
for ray direction (all other data is invalid, as there is no actual
intersection point)


* Library functions *

clTracer shaders support OpenCL 1.1 runtime library, with addition
of common graphics functions (lerp, slerp, bislerp deg2rad, rad2deg)
and accessors (texture, normal and space coordinates from triangle
and barycentric coordinates) and a library of common light models
(lambert, phong). see cl/algs.h and sl/stdshader.h for details


** Compute kernels **

the raytracing algorithm consists of several steps not found in
classic raytracers, mostly due to specifics and limitation of OpenCL

the mail task loop could be simplified in the following way:

cast rays
cast shadow rays
count new rays
allocate memory
evaluate shaders and create new rays
if (new rays > 0) do this again


for casting rays, there are currently two algoritms, one using a grid
acceleration structure, and the other being brute-force.
each of these algorithms implements one function for ray tracing,
which has to find the closest intersection, and shadow ray tracing,
which can finish as soon as a single intersection closer than
target point is found

algorithm selection is performed at run time based on provided
acceleration structure, and thus can be changed during computation,
for example using coherent beam tracing for primary rays and switching
to more general method for secondary rays

ray counting is implemented using counting shaders and atomic addition
operators (required feature in OpenCL 1.1)

in preparation steps (before the tracing loop), the acceleration
structure has to be prepared. because this often requires pointer
manipulation, this step is partly done using OpenCL (as the control
program cannot access pointers to device memory)


trace algorithm has to implement functions with these interfaces:

__kernel void finalize_type(__global struct accelerator_s * accel)

this function should be called as part of preparation steps, this
function has pretty free form arguments, because each algorithm uses
a different number of buffers/numbers

void trace_type(__private struct ray_s ray,
	__global struct hit_s * target_hit,
	__global const struct triangle_s * triangles,
	__global const struct typeaccel_s * accel)

first argument contains information about the ray being currently
traced (origin, direction)

the second argument points to memory for resultant hit (image
reconstruction information and ray direction should be directly
copied from first argument)

third argument represents the scene, as a bunch of triangles

the fourth argument has type specific for each argument, and points
to the acceleration structure used by this algorithm

bool trace_shadow_type(__private float3 origin,
	__private float3 direction,
	__private float maxt,
	__global const struct triangle_s * triangles,
	__global const struct typeaccel_s * accel )

first three arguments describe the shadow ray, as origin,direction
and distance (in that order)

the fourth and fifth arguments are the same as third and fourth
argument of trace_type


** Tiling **

tiling is implemented due to limited memory on OpenCL compute devices
tiles are rendered in order, with new memory setup between each tile -
this slowdown is equivalent to rendering a new frame
performance is hurt badly with small tiles, large tiles cause you to run
out of memory.
memory requirements are about 80B per ray, and 32B per ray and light source.
if you have materials that are both reflective and refractive at the same
time you should multiply the number of rays by 2 (or more)

this should not be more than half of your available memory

space for image result is kept in host memory in one piece in 32bit float RGB,
which is 12B per pixel 


** Interactive mode **

interactive mode uses GLUT to create OpenGL context, and then creates a shared
OpenGL/OpenCL context for data sharing. this is currently implemented for GLX,
other platforms may be available in future

any movement in the interactive mode causes the scene to be re-rendered, and
instead of copying the output to control program memory, it is copied to an
OpenGL pixel buffer, which is in turn used to create OpenGL texture. this works
around problems with sharing texture objects while avoiding costly device to
host memory transfers
