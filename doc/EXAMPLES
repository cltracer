*** Configuration syntax and examples ***


config file syntax:
------------START HERE-------------
(global configuration)

(light configuration)

(per-frame camera configuration)
-----------END HERE----------------

global configuration:
------------START HERE-------------
frames (count)
target (output_filename)
objfile (input_filename)
accelstruct (grid/dummy)
density (number)
resoultion (width) (heigth)
tiles (width) (height)
-----------END HERE----------------

global configuration notes:
density value represents average number of triangles per grid cell
output_filename should include '%d' for multiframe mode
input_filename is a scene in OBJ format, with custom material file
more acceleration structures will be added in future versions
resolution represent size of a single tile
tiles is the number of tiles of the output image
frames value can be 'interactive', in which case frame 0 is the starting point


light configuration:
------------START HERE-------------
lights (count)

light (number) (type) (intensity) [color] (additional params)
-----------END HERE----------------

light configuration notes:
'[]' signalizes triplet, and is included in the file
currently at least 1 light has to be specified
'light' line is repeated for each light in scene


light type and additional params:
ambient - none
omni - [position]
directional - [direction]
spot - [position] [direction] (angle) (cutoff)


camera configuration:
------------START HERE-------------
frame (number)
eyepoint (x) (y) (z)
lookat (x) (y) (z)
upvector (x) (y) (z)
viewangle (w) (h)
-----------END HERE----------------

camera configuration notes:
configuration is repeated fo every frame
option lines (not the 'frame' line) may be omitted, meaning last value


material file syntax:
------------START HERE-------------
(material name) (texture count) (texture 1 file) (texture 2 file) ...
-----------END HERE----------------

material configuration notes:
material file may contain multiple material definitions
material file is referenced from scene file by 'mtllib' keyword
BMPs are loaded in BGR order - correct this in your shader
currently only 24 and 32bit textures are supported
shader definitions are placed in mat/(material name).cl
builtin shaders for background and terminated ray don't use textures


shader file syntax:
------------START HERE-------------
void (material name)
{
 shader code for setting color and creating new rays
}

int (material name)_count
{
 code returning the number of reflected and refracted rays
}

-----------END HERE----------------

shader file syntax notes:
see TECHNICAL for detailed parameter description




config file example:

------------START HERE-------------
frames 3
target image%d.jpg
objfile scene.obj
accelstruct grid
density 2.0
resolution 2048 2048
worksize 16 16

lights 4
light 0 ambient 0.2 [1.0 1.0 1.0]
light 1 omni 100.0 [1.0 0.9 0.8] [15.0 3.0 4.0]
light 2 directional 30.0 [1.0 1.0 1.0] [0.0 -1.0 0.0]
light 3 spot 30.0 [0.0 1.0 0.0] [0.0 -8.0 -6.0] [0.0 1.0 1.0] 30.0 10.0

frame 0
lookat 0.0 0.0 0.0
eyepoint 2.0 2.0 2.0
upvector 0.0 1.0 0.0
viewangle 45.0 45.0
frame 1
eyepoint 3.0 3.0 3.0
viewangle 35.0 35.0
frame 2
lookat 1.0 1.0 1.0
upvector 0.0 1.0 1.0
viewangle 20.0 20.0

-----------END HERE----------------


material file example - scene.mtl:

------------START HERE-------------
wood 1 wood.png
wall 2 bump.png paint.png
-----------END HERE----------------


material definition example - mat/wood.cl:

------------START HERE-------------
#include "sl/stdshader.h"

void wood(__private const struct hit_s * hit, __global const struct triangle_s * triangle, __global struct ray_s * rays, __global struct pixel_s * image,
        __global const struct light_sample_s * light_samples, __global const struct light_s * lights, __const int lightcount, __read_only image2d_t tex)
{
 const sampler_t samp = CLK_NORMALIZED_COORDS_TRUE|CLK_ADDRESS_REPEAT|CLK_FILTER_NEAREST;   
 __private float3 color=read_imagef(tex,samp,textureCoords(triangle,hit->baryCoords)).xyz;	//take color from texture
 __private float lighting = lambert(normalCoords(triangle,hit->baryCoords),hit->rayDirection,light_samples,lights,lightcount);	//use lambert light model
 lockPixel(image[hit->reconstructInfo.pixelNumber]);
 image[hit->reconstructInfo.pixelNumber].color += color * lighting * hit->reconstructInfo.importance;
 image[hit->reconstructInfo.pixelNumber].fill += hit->reconstructInfo.importance;
 unlockPixel(image[hit->reconstructInfo.pixelNumber]);

}

int wood_count(__private const struct hit_s * hit, __global const struct triangle_s * triangle)
{
 return 0;	//we don't reflect anything
}
-----------END HERE----------------
