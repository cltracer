/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * Copyright (C) 2010 Pavel Herrmann (morpheus.ibis@gmail.com)

 */

#include "shared.h"
#include "conf.h"
#include "ocl.h"
#include "interactive.h"
#include <GL/freeglut.h>
#include <CL/cl.h>
#include <CL/cl_gl.h>
#include <GL/glx.h>
#include <stdio.h>
#include <math.h>

#define STEP 0.5f

void glGenBuffers(GLsizei, GLuint*);
void glBindBuffer(GLenum,GLuint);
void glDeleteBuffers(GLsizei, GLuint*);
void glBufferData(GLenum, GLsizeiptr, const GLvoid *, GLenum);


GLuint gl_out;
GLuint gl_pbo;
cl_mem cl_pbo;
int change=1;


extern cl_context ctx;
extern cl_command_queue cq;
extern cl_platform_id pid;
extern cl_device_id device;
extern cl_mem pixels;
extern cl_kernel tools_copytex_kern;
extern cl_event trace;
extern cl_event shade;
extern size_t gwsize[2];
extern size_t imgsize[2];



int opencl_prepare_gl()
{
 error = clGetPlatformIDs(1,&pid,NULL); ERRCHECK
 error = clGetDeviceIDs(pid,CL_DEVICE_TYPE_GPU,1,&device,NULL); ERRCHECK
 cl_context_properties ctxprop[] =
 {
  CL_GL_CONTEXT_KHR, (cl_context_properties)glXGetCurrentContext(),
  CL_GLX_DISPLAY_KHR, (cl_context_properties)glXGetCurrentDisplay(),
  CL_CONTEXT_PLATFORM, (cl_context_properties)pid,
  0
 };
 ctx = clCreateContext(ctxprop,1,&device,NULL,NULL,&error);     ERRCHECK
 cq = clCreateCommandQueue(ctx, device,0,&error);       ERRCHECK
 return 0;
cleanup:
 return error;
}

int copy_texture(int2 curtile)
{
 glFinish();
 gwsize[0]=tilesize[0];
 gwsize[1]=tilesize[1];
 cl_int rowlen = tilesize[0]*tiles.x;
 error=clEnqueueAcquireGLObjects(cq,1,&cl_pbo,0,NULL,&shade);   ERRCHECK
 error=clSetKernelArg(tools_copytex_kern,0,sizeof(cl_mem),&pixels);     ERRCHECK
 error=clSetKernelArg(tools_copytex_kern,1,sizeof(cl_int2),&curtile);   ERRCHECK
 error=clSetKernelArg(tools_copytex_kern,2,sizeof(cl_int),&rowlen);     ERRCHECK
 error=clSetKernelArg(tools_copytex_kern,3,sizeof(cl_mem),&cl_pbo);     ERRCHECK
 error=clEnqueueNDRangeKernel(cq,tools_copytex_kern,2,NULL,gwsize,lwsize,1,&shade,&trace);      ERRCHECK
 error=clWaitForEvents(1,&trace);       ERRCHECK
 error=clEnqueueReleaseGLObjects(cq,1,&cl_pbo,1,&trace,&shade); ERRCHECK
 error=clWaitForEvents(1,&shade);       ERRCHECK
 error=clFinish(cq);    ERRCHECK
printf("image-to-pbo complete\n");
 glBindBuffer(GL_PIXEL_UNPACK_BUFFER_ARB, gl_pbo);
 glBindTexture(GL_TEXTURE_2D,gl_out);
 glTexSubImage2D(GL_TEXTURE_2D,0,tilesize[0]*curtile.x,tilesize[1]*curtile.y,tilesize[0],tilesize[1],GL_RGBA,GL_UNSIGNED_BYTE,NULL);
 return 0;
cleanup:
 return error;
}



void gl_display(void)
{
 if (change)
 {
  cl_int2 curtile;
  for (curtile.y=0; curtile.y<tiles.y; curtile.y++)
  {
   for (curtile.x=0; curtile.x<tiles.x; curtile.x++)
   {
    opencl_display(curtile);
    copy_texture(curtile);
   }
  }
  change=0;
 }

 glClear(GL_COLOR_BUFFER_BIT);
 glEnable(GL_TEXTURE_2D);
 glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
 glBindTexture(GL_TEXTURE_2D,gl_out);

 glBegin(GL_QUADS);
  glTexCoord2i(0,0);
  glVertex2i(0,0);
  glTexCoord2i(0,1);
  glVertex2i(0,1);
  glTexCoord2i(1,1);
  glVertex2i(1,1);
  glTexCoord2i(1,0);
  glVertex2i(1,0);
 glEnd();
 glBindTexture(GL_TEXTURE_2D,0);

 glFlush();
 glFinish();
 glutSwapBuffers();

}

void normalize (float3 * vec)
{
 float len = sqrt(vec->x*vec->x + vec->y*vec->y + vec->z*vec->z);
 vec->x/=len;
 vec->y/=len;
 vec->z/=len;
}

void gl_key(unsigned char c, int x, int y)
{
 float3 sidevector;
 float3 forvector;
 float3 upvector=current_frame->upvector;


 forvector.x=current_frame->lookat.x - current_frame->eyepoint.x;
 forvector.y=current_frame->lookat.y - current_frame->eyepoint.y;
 forvector.z=current_frame->lookat.z - current_frame->eyepoint.z;

 sidevector.x=upvector.y*forvector.z - upvector.z*forvector.y;
 sidevector.y=upvector.z*forvector.x - upvector.x*forvector.z;
 sidevector.z=upvector.x*forvector.y - upvector.y*forvector.x;


 forvector.x=upvector.z*sidevector.y - upvector.y*sidevector.z;
 forvector.y=upvector.x*sidevector.z - upvector.z*sidevector.x;
 forvector.z=upvector.y*sidevector.x - upvector.x*sidevector.y;


 normalize(&sidevector);
 normalize(&forvector);
 normalize(&upvector);

 switch (c)
 {
  case 'q':
   glutLeaveMainLoop();
   return;
  case 'w':
   current_frame->lookat.x+=STEP*forvector.x;
   current_frame->lookat.y+=STEP*forvector.y;
   current_frame->lookat.z+=STEP*forvector.z;
   current_frame->eyepoint.x+=STEP*forvector.x;
   current_frame->eyepoint.y+=STEP*forvector.y;
   current_frame->eyepoint.z+=STEP*forvector.z;
   break;
  case 's':
   current_frame->lookat.x+=STEP*forvector.x;
   current_frame->lookat.y+=STEP*forvector.y;
   current_frame->lookat.z+=STEP*forvector.z;
   current_frame->eyepoint.x-=STEP*forvector.x;
   current_frame->eyepoint.y-=STEP*forvector.y;
   current_frame->eyepoint.z-=STEP*forvector.z;
   break;
  case 'a':
   current_frame->eyepoint.x+=STEP*sidevector.x;
   current_frame->eyepoint.y+=STEP*sidevector.y;
   current_frame->eyepoint.z+=STEP*sidevector.z;
  case 'z':
   current_frame->lookat.x+=STEP*sidevector.x;
   current_frame->lookat.y+=STEP*sidevector.y;
   current_frame->lookat.z+=STEP*sidevector.z;
   break;
  case 'd':
   current_frame->eyepoint.x-=STEP*sidevector.x;
   current_frame->eyepoint.y-=STEP*sidevector.y;
   current_frame->eyepoint.z-=STEP*sidevector.z;
  case 'x':
   current_frame->lookat.x-=STEP*sidevector.x;
   current_frame->lookat.y-=STEP*sidevector.y;
   current_frame->lookat.z-=STEP*sidevector.z;
   break;
  case 'e':
   current_frame->lookat.x+=STEP*upvector.x;
   current_frame->lookat.y+=STEP*upvector.y;
   current_frame->lookat.z+=STEP*upvector.z;
   current_frame->eyepoint.x+=STEP*upvector.x;
   current_frame->eyepoint.y+=STEP*upvector.y;
   current_frame->eyepoint.z+=STEP*upvector.z;
   break;
  case 'c':
   current_frame->lookat.x-=STEP*upvector.x;
   current_frame->lookat.y-=STEP*upvector.y;
   current_frame->lookat.z-=STEP*upvector.z;
   current_frame->eyepoint.x-=STEP*upvector.x;
   current_frame->eyepoint.y-=STEP*upvector.y;
   current_frame->eyepoint.z-=STEP*upvector.z;
   break;
  default:
   return;
 }
 change=1;
 glutPostRedisplay();
}


void gl_timer(int i)
{
 glutPostRedisplay();
 glutTimerFunc(100,gl_timer,i);
}


void interactive_run(int * argc, char** argv )
{
 glutInit(argc,argv);
 glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
 glutInitWindowSize(imgsize[0], imgsize[1]);
 glutCreateWindow("clTracer");

 error=opencl_prepare_gl();     ERRCHECK
 error=prepare_buffers_kernels();      ERRCHECK

 glutDisplayFunc(gl_display);
 glutKeyboardFunc(gl_key);
 glutTimerFunc(100,gl_timer,0);
 glViewport(0, 0, imgsize[0],imgsize[1]);
 glMatrixMode(GL_PROJECTION);
 glLoadIdentity();
 glOrtho(0,1,0,1,0,100);
 glClearColor(0.0f,0.0f,0.0f,0.0f);
 glGenTextures(1, &gl_out);
 glBindTexture(GL_TEXTURE_2D,gl_out);
 glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
 glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
 glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,imgsize[0],imgsize[1],0,GL_RGBA,GL_UNSIGNED_BYTE,NULL);
 glBindTexture(GL_TEXTURE_2D,0);
 glGenBuffers(1, &gl_pbo);
 glBindBuffer(GL_ARRAY_BUFFER,gl_pbo);
 glBufferData(GL_ARRAY_BUFFER,tilesize[0]*tilesize[1]*sizeof(cl_uchar4), NULL, GL_DYNAMIC_DRAW);

 glBindBuffer(GL_ARRAY_BUFFER,0);
 glFinish();

 cl_pbo=clCreateFromGLBuffer(ctx, CL_MEM_READ_WRITE,gl_pbo,&error);    ERRCHECK;
 error=clFinish(cq);   ERRCHECK

 glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_CONTINUE_EXECUTION);
 glutMainLoop();

 cleanup:
 if (cl_pbo!=NULL)
 {
  clReleaseMemObject(cl_pbo);
  cl_pbo=NULL;
 }
 glDeleteBuffers(1,&gl_pbo);
 glDeleteBuffers(1,&gl_out);


}
