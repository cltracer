/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * Copyright (C) 2010 Pavel Herrmann (morpheus.ibis@gmail.com)

 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "types.h"
#include "textures.h"
#include <IL/il.h>

#define L(cstbuf) sizeof(cstbuf)-1
#define scpy(target,source) strncpy(target+target##idx,source, L(source));target##idx+=L(source)


char* createShadeKernel(int mtlnum, struct mtl_def_s * materials, char** mtllist)
{
 const char types[]="#include \"types.h\"\n#include \"cl/algs.h\"\n#include \"mat/background.cl\"\n";
 const char inc[]="#include \"mat/";
 const char sep[]=".cl\"\n";
 const char shade_head[]="__kernel void shade_kernel(__global struct hit_s * hits, __const int raycount,\n"
	"__global const int * rayindex, __global struct ray_s * rays, __global struct pixel_s * image,\n"
	"__global const struct light_sample_s * light_samples, __global const struct light_s * lights, __const int lightcount,\n"
	"__global const struct triangle_s * triangles, __global const struct accel_s * accel";
 const char count_head[]="__kernel void shade_count_kernel(__global struct hit_s * hits, __const int raycount, __global int * rayindex,\n"
	"__global int * newcount, __global const struct triangle_s * triangles";
 const char prepare[]="__private int hitnum = get_global_id(0)+get_global_id(1)*get_global_size(0);\n"
	"if (hitnum >= raycount) return;\n"
	"__private struct hit_s hit = hits[hitnum];\n";
 const char shade_bkgnd[]="if (islive(&(hit.reconstructInfo))==0) return;\nif(hit.triIndex<0){\n"
	"background(&hit, image);\n}else{\n";
 const char count_bkgnd[]="if (islive(&(hit.reconstructInfo))==0) hit.triIndex=-1;\nif(hit.triIndex>=0){\n";
 const char prepare_valid[]="__global const struct triangle_s * triangle = &(triangles[hit.triIndex]);\n"
	"__private int materialindex = triangle->objectIndex;\n";
 const char count_begin[]="__private int cnt=0;\n";
 const char begin[]="switch(materialindex) {\n";
 const char params[]="(&hit, triangle";
 const char shade_params[]=", &(rays[rayindex[hitnum]]), image, &(light_samples[hitnum*lightcount]), lights, lightcount";
 const char count_work[]="cnt=";
 const char ret[]="return;\n";
 const char brk[]="break;\n";
 const char block_begin[]=")\n{\n";
 const char shade_end[]="}}\n}\n";
 const char count_postfix[]="_count";
 const char count_end[]="}}\n__local volatile int lcnt;\n__local int base;\n__private int offset=0;\nlcnt=0;\n"
	"barrier(CLK_LOCAL_MEM_FENCE);\noffset=atomic_add(&lcnt,cnt);\nbarrier(CLK_LOCAL_MEM_FENCE);\n"
	"if ((get_local_id(0)==0)&&(get_local_id(1)==0))\n base=atomic_add(newcount,lcnt);\nbarrier(CLK_LOCAL_MEM_FENCE);\n"
	"rayindex[hitnum]=base+offset;\n}\n";
 const char work_end[]=");\n";

 const char csf[]="case %3d:\n";	//10 chars
 const char txf[]=", __read_only image%dd_t t%03d";	//28 chars
 const char tf[]=", t%03d";	//6 chars

 char * retval;
 char * head;
 int headlen;
 int headidx;
 char * shade;
 int shadelen;
 int shadeidx;
 char * count;
 int countlen;
 int countidx;

 int t;
 int texcount=-1;

 headlen = L(types);
 shadelen = L(shade_head) + L(block_begin) + L(prepare) + L(shade_bkgnd) + L(prepare_valid) + L(begin) + L(shade_end);
 countlen = L(count_head) + L(block_begin) + L(prepare) + L(count_bkgnd) + L(prepare_valid) + L(count_begin) + L(begin) + L(count_end);
 for (int i=0;i<mtlnum;i++)
 {
  t=strlen(mtllist[i]);
  headlen+=L(inc) + t + L(sep);
  countlen+=10/*L(csf)*/ + L(count_work) + t + L(count_postfix) + L(params) + L(work_end) + L(brk);
  shadelen+=10/*L(csf)*/ + t + L(params) + L(shade_params) + L(work_end) + L(ret);
  for (int j=0;j<materials[i].texcount;j++)
  {
   shadelen+=6/*L(tf)*/;
   if(texcount<materials[i].texidx[j]) texcount=materials[i].texidx[j];
  }
 }
 shadelen+=28/*L(txf)*/ * (texcount+1);

 head = malloc(sizeof(char)* (headlen+1));
 shade = malloc(sizeof(char)* (shadelen+1));
 count = malloc(sizeof(char)* (countlen+1));

 head[headlen]=0;
 shade[shadelen]=0;
 count[countlen]=0;

 headidx=0;
 countidx=0;
 shadeidx=0;

 scpy(head,types);

 scpy(count,count_head);
 scpy(count,block_begin);
 scpy(count,count_begin);
 scpy(count,prepare);
 scpy(count,count_bkgnd);
 scpy(count,prepare_valid);
 scpy(count,begin);

 scpy(shade,shade_head);

 for (int i=0;i<texcount+1;i++)
 {
  sprintf(shade+shadeidx,txf,2,i);
  shadeidx+=28;
 }
 scpy(shade,block_begin);
 scpy(shade,prepare);
 scpy(shade,shade_bkgnd);
 scpy(shade,prepare_valid);
 scpy(shade,begin);


 for (int i=0;i<mtlnum;i++)
 {
  t=strlen(mtllist[i]);
  scpy(head,inc);
  strncpy(head+headidx,mtllist[i],t);
  headidx+=t;
  scpy(head,sep);

  sprintf(count+countidx,csf,i);
  countidx+=10;
  scpy(count,count_work);
  strncpy(count+countidx,mtllist[i],t);
  countidx+=t;
  scpy(count,count_postfix);
  scpy(count,params);
  scpy(count,work_end);
  scpy(count,brk);

  sprintf(shade+shadeidx,csf,i);
  shadeidx+=10;
  strncpy(shade+shadeidx,mtllist[i],t);
  shadeidx+=t;
  scpy(shade,params);
  scpy(shade,shade_params);

  for (int j=0;j<materials[i].texcount;j++)
  {
   sprintf(shade+shadeidx,tf,materials[i].texidx[j]);
   shadeidx+=6;
  }
  scpy(shade,work_end);
  scpy(shade,ret);

 }
 scpy(count,count_end);
 scpy(shade,shade_end);


 retval = malloc(sizeof(char)*(headlen+shadelen+countlen+1));
 strncpy(retval,head,headlen);
 strncpy(retval+headlen,shade,shadelen);
 strncpy(retval+headlen+shadelen,count,countlen);
 retval[headlen+shadelen+countlen]=0;

//printf("%s\n",retval);
 return retval;
}


int loadTextures(int mtlnum, struct texinfo_s ** textures, struct mtl_def_s * materials)
{
 int texnum=0;
 ilInit();
 ILuint img;
 ILubyte * imgdata;
 size_t imglength;
 ilGenImages(1,&img);
 ilBindImage(img);
 for (int i=0;i<mtlnum;i++)
 {
  materials[i].texidx = malloc(sizeof(int)*materials[i].texcount);
  for (int j=0;j<materials[i].texcount;j++)
   materials[i].texidx[j]=texnum+j;
  texnum+=materials[i].texcount;
 }
 *textures = malloc(sizeof(struct texinfo_s)*texnum);
 texnum=0;
 for (int i=0;i<mtlnum;i++)
  for (int j=0;j<materials[i].texcount;j++)
  {
   ilLoadImage(materials[i].texnames[j]);
   imgdata=ilGetData();
   (*textures)[texnum].bpp=ilGetInteger(IL_IMAGE_BITS_PER_PIXEL);
   (*textures)[texnum].dimensions.x=ilGetInteger(IL_IMAGE_WIDTH);
   (*textures)[texnum].dimensions.y=ilGetInteger(IL_IMAGE_HEIGHT);
   (*textures)[texnum].dimensions.z=ilGetInteger(IL_IMAGE_DEPTH);
   imglength=((*textures)[texnum].dimensions.x * (*textures)[texnum].dimensions.y * (*textures)[texnum].dimensions.z * 4);
   (*textures)[texnum].data=malloc(sizeof(unsigned char)*imglength);

   switch (ilGetInteger(IL_IMAGE_BITS_PER_PIXEL))
   {
    case 24:
     for (int k=0;k<(imglength/4);k++)
     {
     (*textures)[texnum].data[k*4+0] = imgdata[k*3+0];
     (*textures)[texnum].data[k*4+1] = imgdata[k*3+1];
     (*textures)[texnum].data[k*4+2] = imgdata[k*3+2];
     (*textures)[texnum].data[k*4+3] = 255;
     }
     break;
    case 32:
     memcpy((*textures)[texnum].data,imgdata,imglength);
     break;
    default:
     exit(2);
   }
   (*textures)[texnum].bpp=32;
   texnum++;
  }
 return texnum;
}
