#include "types.h"

int islive (__private struct reconstruct_s * ray)
{
 if (ray->importance > 0.001f)
  return 1;
 return 0;
}

void background(__private struct hit_s * hit, __global struct pixel_s * image)
{
 __private float3 color = hit->rayDirection.y*0.5f;
 lockPixel(image[hit->reconstructInfo.pixelNumber]);
  image[hit->reconstructInfo.pixelNumber].color += color * hit->reconstructInfo.importance;
  image[hit->reconstructInfo.pixelNumber].fill += hit->reconstructInfo.importance;
 unlockPixel(image[hit->reconstructInfo.pixelNumber]);
}
