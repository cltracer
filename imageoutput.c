/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * Copyright (C) 2010 Pavel Herrmann (morpheus.ibis@gmail.com)

 */

#include <IL/il.h>


int writeImage(char* name, int width, int height, float * data)
{
 ilInit();
 ILuint img;
 ilGenImages(1,&img);
 ilBindImage(img);
 ilTexImage(width,height,1,3,IL_RGB,IL_FLOAT,data);
 ilEnable(IL_FILE_OVERWRITE);
 ilSaveImage(name);
}


