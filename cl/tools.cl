
/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * Copyright (C) 2010 Pavel Herrmann (morpheus.ibis@gmail.com)

 */

#include "types.h"
#include "cl/algs.h"

__kernel void fix_normals(__global struct triangle_s * triangles, int tricount)
{
 int trinum = get_global_id(0);
 if (trinum >= tricount)
  return;
 __private struct triangle_s local_tri = triangles[trinum];
 local_tri.normalCoords[0]=normalize(local_tri.normalCoords[0]);
 local_tri.normalCoords[1]=normalize(local_tri.normalCoords[1]);
 local_tri.normalCoords[2]=normalize(local_tri.normalCoords[2]);
 triangles[trinum]=local_tri;
 return;
}

__kernel void fix_light_direction (__global struct light_s * lights, int lightcount)
{
 int lightnum = get_global_id(0);
 if (lightnum >= lightcount)
  return;
 switch (lights[lightnum].type)
 {
  case LIGHT_DIRECTIONAL:
   lights[lightnum].directional.direction=normalize(lights[lightnum].directional.direction);
   break;
  case LIGHT_SPOT:
   lights[lightnum].spot.direction=normalize(lights[lightnum].spot.direction);
   break;
 }
 return;
}

__kernel void gen_primary_rays (__global struct ray_s * rays, float3 eyepoint, float3 lookat, float3 upvector, float2 angle, int2 curtile, int2 maxtile, __global unsigned int * seeds)
{
 __private int raynum = get_global_id(0) + get_global_id(1)*get_global_size(0);
 __local float3 imageplanenormal;
 imageplanenormal = eyepoint-lookat;
 __local float3 xdest;
 __local float3 ydest;
 __local float2 targetlength;
 xdest = cross(upvector,imageplanenormal);
 ydest = cross(imageplanenormal,xdest);
 targetlength.x = tan(deg2rad(angle.x)/2)*length(imageplanenormal);
 targetlength.y = tan(deg2rad(angle.y)/2)*length(imageplanenormal);
 ydest=ydest*(targetlength.y/length(ydest));
 xdest=xdest*(targetlength.x/length(xdest));
 __private struct ray_s local_ray;
 local_ray.origin=eyepoint;
 local_ray.direction= -imageplanenormal;
 local_ray.direction+=(((( (get_global_id(0)+curtile.x*get_global_size(0)) +0.5f)/(get_global_size(0)*maxtile.x))*2)-1.0f)*xdest;
 local_ray.direction+=(((( (get_global_id(1)+curtile.y*get_global_size(1)) +0.5f)/(get_global_size(1)*maxtile.y))*2)-1.0f)*ydest;
 local_ray.direction=normalize(local_ray.direction);
 local_ray.reconstructInfo.randomSeed=seeds[raynum];
 local_ray.reconstructInfo.pixelNumber=raynum;
 local_ray.reconstructInfo.importance=1.0f;
 rays[raynum]=local_ray;
}

__kernel void copy_image_to_pbo(__global const struct pixel_s * image, int2 curtile, int rowsize, __global uchar4 * pbo)
{
 __private int incoord = get_global_id(1)*get_global_size(0) + get_global_id(0);

 __private uchar4 color;
 color.xyz=convert_uchar3(255*image[incoord].color);

 pbo[incoord] = color;
}
