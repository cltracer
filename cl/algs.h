
/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * Copyright (C) 2010 Pavel Herrmann (morpheus.ibis@gmail.com)

 */

#ifndef _ALGS_H_
#define _ALGS_H_ 1
#include "types.h"


#define EPSILON_DOT 0.001f	//slerp->lerp fallback control
#ifndef M_PI
#define M_PI 3.14159265359f
#endif


inline float deg2rad(float deg)
{
 return (deg * (M_PI/180));
}

inline float rad2deg(float rad)
{
 return (rad * (180/M_PI));
}

inline float lerp(float a, float b, float t)
{
 return a*t + b*(1-t);
}

inline float bilerp(float a, float b, float c, float t, float s)
{
 return a*t + b*s + c*(1-(t+s));
}

inline float2 lerp2(float2 a, float2 b, float t)
{
 return a*t + b*(1-t);
}

inline float2 bilerp2(float2 a, float2 b, float2 c, float t, float s)
{
 return a*t + b*s + c*(1-(t+s));
}

inline float3 lerp3(float3 a, float3 b, float t)
{
 return a*t + b*(1-t);
}

inline float3 bilerp3(float3 a, float3 b, float3 c, float t, float s)
{
 return a*t + b*s + c*(1-(t+s));
}

inline float2 slerp2(float2 a, float2 b, float t)
{
 if (fabs(dot(a,b)-1.0f)<EPSILON_DOT)
  return lerp2(a,b,t);		//slerp on flat surfaces is broken (division by zero), use lerp instead
 __private float omega=acos(dot(a,b));
 return a*sin(t*omega)/sin(omega) + b*sin((1-t)*omega)/sin(omega);
}

inline float2 bislerp2(float2 a, float2 b, float2 c, float t, float s)
{
 return slerp2(c,slerp2(a,b,t/(t+s)),1-(t+s));
}

inline float3 slerp3(float3 a, float3 b, float t)
{
 if (fabs(dot(a,b)-1.0f)<EPSILON_DOT)
  return lerp3(a,b,t);		//slerp on flat surfaces is broken (division by zero), use lerp instead
 __private float omega=acos(dot(a,b));
 return a*sin(t*omega)/sin(omega) + b*sin((1-t)*omega)/sin(omega);
}

inline float3 bislerp3(float3 a, float3 b, float3 c, float t, float s)
{
 return slerp3(c,slerp3(a,b,t/(t+s)),1-(t+s));
}

float intersectRayTri(__private struct ray_s * ray, __private struct triangle_s * triangle, __private float3 * baryCoords)
{
 __private float3 e1=triangle->spaceCoords[1]-triangle->spaceCoords[0];
 __private float3 e2=triangle->spaceCoords[2]-triangle->spaceCoords[0];

 __private float3 N = cross(e1,e2);
 __private float distance = -dot((ray->origin - triangle->spaceCoords[0]),N)/dot(ray->direction,N);
 N=fabs(N);
 __private float maxdim=fmax(N.x,fmax(N.y,N.z));
 __private float3 P=ray->origin+(distance*ray->direction);
 __private float3 p3;
 __private float2 b,c,p;

 p3=(P-triangle->spaceCoords[0]);

 if (N.x==maxdim)
 {
  b=e1.yz;
  c=e2.yz;
  p=p3.yz;
 }
 else if (N.y==maxdim)
 {
  b=e1.xz;
  c=e2.xz;
  p=p3.xz;
 }
 else
 {
  b=e1.xy;
  c=e2.xy;
  p=p3.xy;
 }
 __private float3 bary;

 bary.y=( ((p.y)*(c.x)) - ((p.x)*(c.y)) ) / ( ((b.y)*(c.x)) - ((b.x)*(c.y)) );
 bary.z=( ((p.y)*(b.x)) - ((p.x)*(b.y)) ) / ( ((c.y)*(b.x)) - ((c.x)*(b.y)) );
 bary.x=( 1 - ((bary.y)+(bary.z)) );

// if ((bary.x<0.0f)||(bary.y<0.0f)||(bary.z<0.0f))
 if (any(isless(bary,(float3)(0.0f))))
  return -1.0f;		//INFINITY is not a good idea
 *baryCoords=bary;
 return distance;
}

float2 textureCoords(__global const struct triangle_s * tri, const float3 bary)
{
 return bilerp2(tri->textureCoords[0],tri->textureCoords[1],tri->textureCoords[2],bary.s0,bary.s1);
}

float3 normalCoords(__global const struct triangle_s * tri, const float3 bary)
{
 return bislerp3(tri->normalCoords[0],tri->normalCoords[1],tri->normalCoords[2],bary.s0,bary.s1);
}

float3 spaceCoords(__global const struct triangle_s * tri, const float3 bary)
{
 return bilerp3(tri->spaceCoords[0],tri->spaceCoords[1],tri->spaceCoords[2],bary.s0,bary.s1);
}

#define lockPixel(pix) \
{ \
 while(atomic_xchg(&((pix).lock),1)==0) \
 { \

#define unlockPixel(pix) \
  atomic_xchg(&((pix).lock),0); \
  break; \
 } \
}

/* random number generator adapted from http://www.cs.wm.edu/~va/software/park/park.html */

#define MODULUS    2147483647
#define MULTIPLIER 48271


float random(unsigned int* seed)
{
 const long Q = MODULUS / MULTIPLIER;
 const long R = MODULUS % MULTIPLIER;
 unsigned int t=MULTIPLIER * (*seed % Q) - R * (*seed / Q);
 *seed=t;
 return (( (float)t/MODULUS )-0.5f)*2.0f;
}

/* LCG from wikipedia*/

#define A 1103515245
#define C 12345
#define M 0xffffffff

unsigned int split_random(unsigned int seed)
{
 unsigned int t=(A * (seed) + C) % M;
 return t;
}

float3 random_f3n(unsigned int* seed)
{
 float3 ret;
 ret.x=random(seed);
 ret.y=random(seed);
 ret.z=random(seed);
 ret=normalize(ret);
 return ret;
}


#endif //_ALGS_H_
