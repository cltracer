/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * Copyright (C) 2010 Pavel Herrmann (morpheus.ibis@gmail.com)

 */

#include "types.h"
#include "cl/trace.cl"

struct light_sample_s illuminate(__global struct light_s * light, __private float3 target,
	__global const struct triangle_s * triangles, __global const struct accel_s * accel)
{
 __private struct light_sample_s retval;
 __private float3 L;
 __private struct light_s local_light = *light;
 __private float angle, maxangle;
 __private float maxt;
 retval.color = local_light.color;
 switch(local_light.type)
 {
  case LIGHT_AMBIENT:
   retval.intensity = local_light.intensity;
   retval.direction = 0.0f;
   return retval;	//early return - no rays for ambient light
  case LIGHT_OMNI:
   retval.direction = normalize( L = ( target - local_light.omni.position ) );
   retval.intensity = local_light.intensity / pown( maxt = length( L ), 2);
   break;
  case LIGHT_DIRECTIONAL:
   retval.direction = L = local_light.directional.direction;
   retval.intensity = local_light.intensity;
   maxt = INFINITY;
   break;
  case LIGHT_SPOT:
   retval.direction = normalize( L = ( target - local_light.spot.position ) );
   retval.intensity = ( local_light.intensity / pown( maxt = length( L ), 2) );
   angle = acos(dot(retval.direction, local_light.spot.direction));
   if ( angle > deg2rad(maxangle = local_light.spot.coneangle + local_light.spot.cutoff) )
   {
    retval.intensity = 0.0f;
    return retval;	//early return - target out of light cone
   }
   retval.intensity *= 1.0f - smoothstep(deg2rad(local_light.spot.coneangle), deg2rad(maxangle), angle);
   break;
 }

 
 //trace shadow ray
 if (!trace_shadow(target, normalize( -L ), maxt, triangles, accel))
  retval.intensity = 0.0f;
 
 return retval;
}




__kernel void light_kernel (__global struct hit_s * hits, __const int hitcount, __global struct light_sample_s * samples,
	__global struct light_s * lights, __const int lightcount, __global const struct triangle_s * tris, __global const struct accel_s * accel)
{
 __private int hitnum=get_global_id(0)+get_global_id(1)*get_global_size(0);
 if (hitnum >= hitcount)
  return;
 if (hits[hitnum].triIndex<0)
  return;
 for (int l=0;l<lightcount;l++)
 {
  samples[(hitnum*lightcount)+l]=illuminate(&(lights[l]),spaceCoords(&(tris[hits[hitnum].triIndex]),hits[hitnum].baryCoords), tris, accel);
 }

}
