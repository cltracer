
/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * Copyright (C) 2010 Pavel Herrmann (morpheus.ibis@gmail.com)

 */

#include "types.h"
#include "cl/algs.h"

//fix a bug with float comparison
#define EPSILON 0.00001f

__kernel void finalize_dummy (__global struct accel_s * accelerator, int tricount)
{
 accelerator->type=ACCEL_DUMMY;
 accelerator->dummy.triangleCount=tricount;
}



void trace_dummy(__private struct ray_s local_ray, __global struct hit_s * target_hit, 
	__global const struct triangle_s * triangles, __global const struct dummyaccel_s * accel )
{
 __private float maxt=INFINITY;
 __private float t=0.0f;
 __private float3 tbary;
 __private struct triangle_s ttri;
 __private int tricount;
 tricount=accel->triangleCount;
 __private struct hit_s hit;
 hit.triIndex=-1;
 hit.rayDirection=normalize(local_ray.direction);
 hit.reconstructInfo=local_ray.reconstructInfo;
 hit.baryCoords=0.0f;
 for (__private int i=0;i<tricount;i++)
 {
  ttri=triangles[i];
  t=intersectRayTri(&local_ray,&ttri,&tbary);
  if ((t<maxt)&&(t>EPSILON))
  {
   maxt=t;
   hit.triIndex=i;
   hit.baryCoords=tbary;
  }
 }
 *target_hit=hit;
 return;
}


bool trace_shadow_dummy(__private float3 origin, __private float3 direction, __private float maxt,
	__global const struct triangle_s * triangles, __global const struct dummyaccel_s * accel )
{
 __private float t=0.0f;
 __private float3 tbary;
 __private struct triangle_s ttri;
 __private struct ray_s local_ray;
 __private int tricount;
 local_ray.origin=origin;
 local_ray.direction=direction;
 tricount=accel->triangleCount;
 for (int i=0;i<tricount;i++)
 {
  ttri=triangles[i];
  t=intersectRayTri(&local_ray,&ttri,&tbary);
  if ((t<maxt)&&(t>EPSILON))
  {
   return false;
  }
 }
 return true;
}
