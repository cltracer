
/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * Copyright (C) 2010 Pavel Herrmann (morpheus.ibis@gmail.com)

 */

#include "types.h"
#include "cl/grid.cl"
#include "cl/dummy.cl"


void trace(__private struct ray_s ray, __global struct hit_s * target_hit,
	__global const struct triangle_s * triangles, __global const struct accel_s * accel)
{

 switch (accel->type)
 {
  case ACCEL_DUMMY:
   trace_dummy(ray, target_hit, triangles, (__global struct dummyaccel_s *) &(accel->dummy));
   return;
  case ACCEL_GRID:
   trace_grid(ray, target_hit, triangles, (__global struct gridaccel_s *) &(accel->grid));
   return;
  default:
   return;
 }

}

bool trace_shadow(__private float3 origin, __private float3 direction, __private float maxt,
	__global const struct triangle_s * triangles, __global const struct accel_s * accel)
{
 switch (accel->type)
 {
  case ACCEL_DUMMY:
   return trace_shadow_dummy(origin, direction, maxt, triangles, (__global struct dummyaccel_s *) &(accel->dummy));
  case ACCEL_GRID:
   return trace_shadow_grid(origin, direction, maxt, triangles, (__global struct gridaccel_s *) &(accel->grid));
 }
 return false;
}


__kernel void trace_kernel( __global const struct ray_s * rays, int raycount, __global struct hit_s * hits,
	__global const struct triangle_s * triangles, __global const struct accel_s * accel)
{
 __private int raynum = get_global_id(0)+get_global_id(1)*get_global_size(0);
 if (raynum >= raycount)
  return;
 trace(rays[raynum], &(hits[raynum]),triangles, accel);
 return;
}
