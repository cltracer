/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * Copyright (C) 2010 Pavel Herrmann (morpheus.ibis@gmail.com)

 */

#include "shared.h"
#include "conf.h"
#include "textures.h"
#include <stdio.h>
#include <string.h>

#define PROGCHECK(Part) if (error!=0) { char errmsg[20481]; clGetProgramBuildInfo(Part##_prog,device,CL_PROGRAM_BUILD_LOG,20480,&errmsg,&retnum); printf ("error %d in clBuildProgram on line %d, %zd bytes:\n%s\n",error,__LINE__,retnum,errmsg); goto cleanup; }

#define CLRELEASE(type,object) if (object!=NULL) { clRelease##type(object); object=NULL; }

cl_int memcount=0;
cl_mem rays=NULL;
cl_mem tris=NULL;
cl_mem hits=NULL;
cl_mem pixels=NULL;
cl_mem accel=NULL;
cl_mem ray_number=NULL;
cl_mem ray_indexes=NULL;
cl_mem lights=NULL;
cl_mem light_samples=NULL;
cl_mem seeds=NULL;
cl_mem* hidden_buffers=NULL;
cl_mem* hidden_textures=NULL;
int error;

cl_int pixcount;
size_t gwsize[2];
extern int texnum;

cl_context ctx=NULL;
cl_command_queue cq=NULL;
cl_platform_id pid=NULL;
cl_device_id device=NULL;

cl_program trace_prog=NULL;
cl_kernel trace_kern=NULL;
size_t trace_prog_size=0;
char* trace_prog_text=NULL;

cl_program light_prog=NULL;
cl_kernel light_kern=NULL;
size_t light_prog_size=0;
char* light_prog_text=NULL;

cl_program shade_prog=NULL;
cl_kernel shade_kern=NULL;
cl_kernel shade_count_kern=NULL;
size_t shade_prog_size=0;
char* shade_prog_text=NULL;

cl_program tools_prog=NULL;
cl_kernel tools_normals_kern=NULL;
cl_kernel tools_prirays_kern=NULL;
cl_kernel tools_lights_kern=NULL;
cl_kernel tools_copytex_kern=NULL;
size_t tools_prog_size=0;
char* tools_prog_text=NULL;

cl_program accel_prog=NULL;
cl_kernel accel_kern=NULL;
size_t accel_prog_size=0;
char* accel_prog_text=NULL;

 //clearpixels,tripush,accel,normals,raycast,textures,seeds
cl_event scene_prepare[7]={NULL,NULL,NULL,NULL,NULL,NULL,NULL};
 //grid,cells,gridinfo
cl_event grid_prepare[3]={NULL,NULL,NULL};
cl_event prepare=NULL;
cl_event trace=NULL;
cl_event light=NULL;
cl_event shade=NULL;
cl_event count=NULL;
cl_event * push_texture=NULL;

 //just to make nvidia profiler happy
cl_event extraevent=NULL;


unsigned int * randomseeds=NULL;


extern cl_int tricount, mtlcount;
extern struct triangle_s * triangles;
extern char** mtllist;
extern char* mtllib;
extern cl_int* grid_data;
extern cl_int* cell_data;
extern struct gridinfo_s gridinfo;
extern struct mtl_def_s * materials;
extern struct texinfo_s * textures;
extern int gridcount, cellcount;
struct pixel_s * image=NULL;


int opencl_prepare(void)
{
 error = clGetPlatformIDs(1,&pid,NULL); ERRCHECK
 error = clGetDeviceIDs(pid,CL_DEVICE_TYPE_DEFAULT,1,&device,NULL); ERRCHECK
 ctx = clCreateContext(0,1,&device,NULL,NULL,&error);   ERRCHECK
 cq = clCreateCommandQueue(ctx, device,0,&error);       ERRCHECK
 return 0;
cleanup:
 return error;
}

static char* oclLoadProgSource(char* filename, char* b, size_t* size)
{
 char * result = NULL;
 FILE * f = fopen(filename, "rb");
 if (f == NULL)
 {
  *size = 0;
  return NULL; // -1 means file opening fail
 }
 fseek(f, 0, SEEK_END);
 *size = ftell(f);
 fseek(f, 0, SEEK_SET);
 result = (char *)malloc(*size+1);
 if (*size != fread(result, sizeof(char), *size, f))
 {
  free(result);
  return NULL; // -2 means file reading fail
 }
 fclose(f);
 (result)[*size] = 0;
 return result;
}


int prepare_buffers_kernels()
{
 gwsize[0]=tilesize[0];
 gwsize[1]=tilesize[1];
 pixcount = gwsize[0]*gwsize[1];

 cl_image_format imgform = {CL_RGBA,CL_UNORM_INT8};
 hidden_textures=malloc(sizeof(cl_mem)*texnum);
 // textures
 for(int i=0;i<texnum;i++)
 {
  if(textures[i].bpp!=32)
  {
   printf("texture %d invalid\n",i);
   error=-1;
   goto cleanup;
  }
  if (textures[i].dimensions.z==1)
  {
   hidden_textures[i]=clCreateImage2D(ctx, CL_MEM_READ_ONLY, &imgform, textures[i].dimensions.x, textures[i].dimensions.y,0,NULL,&error);       ERRCHECK
  }
  else
  {
   hidden_textures[i]=clCreateImage3D(ctx, CL_MEM_READ_ONLY, &imgform, textures[i].dimensions.x, textures[i].dimensions.y, textures[i].dimensions.z,0,0,NULL,&error);   ERRCHECK
  }
 }

 if (accelstruct==ACCEL_GRID)
 {
  //gridinfo,grid,cells
  hidden_buffers=malloc(sizeof(cl_mem)*3);
  //gridinfo
  hidden_buffers[0]=clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(struct gridinfo_s),NULL,&error);      ERRCHECK
  //grid
  hidden_buffers[1]=clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(cl_int)*gridcount,NULL,&error);       ERRCHECK
  //cells
  hidden_buffers[2]=clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(cl_int)*cellcount,NULL,&error);       ERRCHECK
 }


 //triangles
 tris=clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(struct triangle_s)*tricount,NULL,&error);   ERRCHECK
 //pixels
 pixels=clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(struct pixel_s)*pixcount,NULL,&error);    ERRCHECK
 //accel
 accel=clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(struct accel_s),NULL,&error);      ERRCHECK
 ray_number=clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(cl_int),NULL,&error); ERRCHECK

 lights=clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(struct light_s)*lightcount,NULL,&error);  ERRCHECK

 error=clEnqueueWriteBuffer(cq, lights,CL_TRUE,0,sizeof(struct light_s)*lightcount,lightlist,0,NULL,&extraevent);       ERRCHECK

 seeds=clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(unsigned int)*pixcount,NULL,&error);       ERRCHECK

 size_t retnum=0;
 //programs/kernels

 trace_prog_text=oclLoadProgSource("cl/trace.cl","",&trace_prog_size);
 trace_prog=clCreateProgramWithSource(ctx,1,(const char**)&trace_prog_text,&trace_prog_size,&error);    ERRCHECK
 error=clBuildProgram(trace_prog,0,NULL,"-I . -Werror",NULL,NULL);        PROGCHECK(trace)
 trace_kern=clCreateKernel(trace_prog, "trace_kernel", &error); ERRCHECK

 light_prog_text=oclLoadProgSource("cl/light.cl","",&light_prog_size);
 light_prog=clCreateProgramWithSource(ctx,1,(const char**)&light_prog_text,&light_prog_size,&error);    ERRCHECK
 error=clBuildProgram(light_prog,0,NULL,"-I . -Werror",NULL,NULL);        PROGCHECK(light)
 light_kern=clCreateKernel(light_prog, "light_kernel", &error); ERRCHECK

 shade_prog_text=createShadeKernel(mtlcount,materials,mtllist);
 shade_prog_size=strlen(shade_prog_text);
 shade_prog=clCreateProgramWithSource(ctx,1,(const char**)&shade_prog_text,&shade_prog_size,&error);    ERRCHECK
 error=clBuildProgram(shade_prog,0,NULL,"-I . -Werror",NULL,NULL);        PROGCHECK(shade)
 shade_kern=clCreateKernel(shade_prog, "shade_kernel", &error); ERRCHECK
 shade_count_kern=clCreateKernel(shade_prog, "shade_count_kernel", &error);     ERRCHECK

 tools_prog_text=oclLoadProgSource("cl/tools.cl","",&tools_prog_size);
 tools_prog=clCreateProgramWithSource(ctx,1,(const char**)&tools_prog_text,&tools_prog_size,&error);    ERRCHECK
 error=clBuildProgram(tools_prog,0,NULL,"-I . -Werror",NULL,NULL);        PROGCHECK(tools)
 tools_normals_kern=clCreateKernel(tools_prog, "fix_normals", &error);  ERRCHECK
 tools_prirays_kern=clCreateKernel(tools_prog, "gen_primary_rays", &error);     ERRCHECK
 tools_lights_kern=clCreateKernel(tools_prog, "fix_light_direction", &error);   ERRCHECK
 tools_copytex_kern=clCreateKernel(tools_prog, "copy_image_to_pbo", &error);    ERRCHECK

 if (accelstruct==ACCEL_GRID)
 {
  accel_prog_text=oclLoadProgSource("cl/grid.cl","",&accel_prog_size);
  accel_prog=clCreateProgramWithSource(ctx,1,(const char**)&accel_prog_text,&accel_prog_size,&error);   ERRCHECK
  error=clBuildProgram(accel_prog,0,NULL,"-I . -Werror",NULL,NULL);       PROGCHECK(accel)
  accel_kern=clCreateKernel(accel_prog, "finalize_grid", &error);       ERRCHECK
 }
 if (accelstruct==ACCEL_DUMMY)
 {
  accel_prog_text=oclLoadProgSource("cl/dummy.cl","",&accel_prog_size);
  accel_prog=clCreateProgramWithSource(ctx,1,(const char**)&accel_prog_text,&accel_prog_size,&error);   ERRCHECK
  error=clBuildProgram(accel_prog,0,NULL,"-I . -Werror",NULL,NULL);       PROGCHECK(accel)
  accel_kern=clCreateKernel(accel_prog, "finalize_dummy", &error);      ERRCHECK
 }

 printf("kernels loaded and compiled\n");

 image = malloc(sizeof(struct pixel_s)*pixcount);
 push_texture = malloc(sizeof(cl_event)*texnum);
 randomseeds = malloc(sizeof(unsigned int)*pixcount);

 //push textures into GPU
 size_t nullorigin[] = {0,0,0};
 size_t texsize[3];
 for (int i=0;i<texnum;i++)
 {
  texsize[0]=textures[i].dimensions.x;
  texsize[1]=textures[i].dimensions.y;
  texsize[2]=textures[i].dimensions.z;
  error=clEnqueueWriteImage(cq,hidden_textures[i],CL_FALSE,nullorigin,texsize,0,0,textures[i].data,0,NULL,&(push_texture[i]));  ERRCHECK
 }
 if (texnum>0)
  error=clWaitForEvents(texnum,push_texture);   ERRCHECK

 if (accelstruct==ACCEL_GRID)
 {
  error=clEnqueueWriteBuffer(cq,hidden_buffers[0],CL_FALSE,0,sizeof(struct gridinfo_s),&gridinfo,0,NULL,&(grid_prepare[0]));    ERRCHECK
  error=clEnqueueWriteBuffer(cq,hidden_buffers[1],CL_FALSE,0,sizeof(cl_int)*gridcount,grid_data,0,NULL,&(grid_prepare[1]));     ERRCHECK
  error=clEnqueueWriteBuffer(cq,hidden_buffers[2],CL_FALSE,0,sizeof(cl_int)*cellcount,cell_data,0,NULL,&(grid_prepare[2]));     ERRCHECK
  error=clWaitForEvents(3,grid_prepare);        ERRCHECK
 }

 error=clEnqueueWriteBuffer(cq,tris,CL_FALSE,0,sizeof(struct triangle_s)*tricount,triangles,0,NULL,&(scene_prepare[1]));        ERRCHECK

 for (int i=0;i<pixcount;i++)
  randomseeds[i]=random();

 error=clEnqueueWriteBuffer(cq, seeds,CL_TRUE,0,sizeof(unsigned int)*pixcount,randomseeds,0,NULL,&(scene_prepare[6]));  ERRCHECK



//prepare accel structure for use
 if (accelstruct==ACCEL_GRID)
 {
  error=clSetKernelArg(accel_kern,0,sizeof(cl_mem),&accel);     ERRCHECK
  error=clSetKernelArg(accel_kern,1,sizeof(cl_mem),&(hidden_buffers[0]));       ERRCHECK
  error=clSetKernelArg(accel_kern,2,sizeof(cl_mem),&(hidden_buffers[1]));       ERRCHECK
  error=clSetKernelArg(accel_kern,3,sizeof(cl_mem),&(hidden_buffers[2]));       ERRCHECK
  error=clEnqueueTask(cq,accel_kern,3,grid_prepare,&(scene_prepare[2]));        ERRCHECK
 }
 if (accelstruct==ACCEL_DUMMY)
 {
  error=clSetKernelArg(accel_kern,0,sizeof(cl_mem),&accel);     ERRCHECK
  error=clSetKernelArg(accel_kern,1,sizeof(cl_int),&tricount);  ERRCHECK
  error=clEnqueueTask(cq,accel_kern,0,NULL,&(scene_prepare[2]));        ERRCHECK
 }
//fix triangle normals
 size_t trisize=tricount;
 error=clSetKernelArg(tools_normals_kern,0,sizeof(cl_mem),&tris);       ERRCHECK
 error=clSetKernelArg(tools_normals_kern,1,sizeof(cl_int),&tricount);   ERRCHECK
 error=clEnqueueNDRangeKernel(cq,tools_normals_kern,1,NULL,&trisize,NULL,0,NULL,&(scene_prepare[3]));   ERRCHECK

//fix light directions
 size_t lightsize=lightcount;
 error=clSetKernelArg(tools_lights_kern,0,sizeof(cl_mem),&lights);      ERRCHECK
 error=clSetKernelArg(tools_lights_kern,1,sizeof(cl_int),&lightcount);  ERRCHECK
 error=clEnqueueNDRangeKernel(cq,tools_lights_kern,1,NULL,&lightsize,NULL,0,NULL,&(scene_prepare[4]));  ERRCHECK

 return 0;
cleanup:
 return error;
}

#ifdef DEBUG
#include <sys/time.h>
struct timeval starttime, endtime;
unsigned long timediff (struct timeval start, struct timeval end)
{
 unsigned long ret = (end.tv_sec * 1000000 + end.tv_usec);
 ret -= (start.tv_sec * 1000000 + start.tv_usec);
 return ret;
}
unsigned long waitfor_trace=0;
unsigned long waitfor_count=0;
unsigned long waitfor_light=0;
unsigned long waitfor_shade=0;
#endif


static int roundup (int a, int b)
{
 int ret = a/b;
 if (ret*b !=a)
  ret++;
 return ret*b;
}

#define MAX(x,y) (x>y?x:y)

int opencl_display(cl_int2 curtile)
{
 cl_int raycount=pixcount;
 cl_int nextcount=0;

 if (pixcount>memcount)
 {
  cl_int inccount=MAX(2*memcount,pixcount);
  printf("reallocating memory buffers\n");
  CLRELEASE(MemObject,rays);
  rays=clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(struct ray_s)*inccount,NULL,&error);       ERRCHECK
  CLRELEASE(MemObject,hits);
  hits=clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(struct hit_s)*inccount,NULL,&error);       ERRCHECK
  CLRELEASE(MemObject,ray_indexes);
  ray_indexes=clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(cl_int)*inccount,NULL,&error);      ERRCHECK
  CLRELEASE(MemObject,light_samples);
  light_samples=clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(struct light_sample_s)*inccount*lightcount,NULL,&error);  ERRCHECK
  memcount = inccount;
 }

printf("tile %d %d\n",curtile.x,curtile.y);
 raycount=pixcount;
 gwsize[0]=tilesize[0];
 gwsize[1]=tilesize[1];
 memset(image,0,sizeof(struct pixel_s)*pixcount);

 error=clEnqueueWriteBuffer(cq,pixels,CL_FALSE,0,sizeof(struct pixel_s)*pixcount,image,0,NULL,&(scene_prepare[0]));     ERRCHECK
 error=clSetKernelArg(tools_prirays_kern,0,sizeof(cl_mem),&rays);       ERRCHECK
 error=clSetKernelArg(tools_prirays_kern,1,sizeof(float3),&(current_frame->eyepoint));  ERRCHECK
 error=clSetKernelArg(tools_prirays_kern,2,sizeof(float3),&(current_frame->lookat));    ERRCHECK
 error=clSetKernelArg(tools_prirays_kern,3,sizeof(float3),&(current_frame->upvector));  ERRCHECK
 error=clSetKernelArg(tools_prirays_kern,4,sizeof(float2),&(current_frame->angles));    ERRCHECK
 error=clSetKernelArg(tools_prirays_kern,5,sizeof(cl_int2),&curtile);   ERRCHECK
 error=clSetKernelArg(tools_prirays_kern,6,sizeof(cl_int2),&tiles);     ERRCHECK
 error=clSetKernelArg(tools_prirays_kern,7,sizeof(cl_mem),&seeds);      ERRCHECK
 error=clEnqueueNDRangeKernel(cq,tools_prirays_kern,2,NULL,gwsize,lwsize,0,NULL,&(scene_prepare[5]));   ERRCHECK
#ifdef DEBUG
 error=clWaitForEvents(7,scene_prepare);        ERRCHECK
#endif
 printf("environment prepared\n");

 error=clEnqueueMarker(cq,&prepare);    ERRCHECK

  //recurse
 while (raycount>0)
 {
printf("recursively tracing %d rays\n", raycount);
  gwsize[1]=roundup(roundup(raycount,gwsize[0])/gwsize[0],lwsize[1]);

  if (raycount>memcount)
  {
   cl_int inccount=MAX(2*memcount,raycount);
   printf("reallocating memory buffers\n");
   CLRELEASE(MemObject,hits);
   hits=clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(struct hit_s)*inccount,NULL,&error);       ERRCHECK
   CLRELEASE(MemObject,ray_indexes);
   ray_indexes=clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(cl_int)*inccount,NULL,&error);      ERRCHECK
   CLRELEASE(MemObject,light_samples);
   light_samples=clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(struct light_sample_s)*inccount*lightcount,NULL,&error);  ERRCHECK
   memcount = inccount;
  }

  error=clSetKernelArg(trace_kern,0,sizeof(cl_mem),&rays);      ERRCHECK
  error=clSetKernelArg(trace_kern,1,sizeof(cl_int),&raycount);  ERRCHECK
  error=clSetKernelArg(trace_kern,2,sizeof(cl_mem),&hits);      ERRCHECK
  error=clSetKernelArg(trace_kern,3,sizeof(cl_mem),&tris);      ERRCHECK
  error=clSetKernelArg(trace_kern,4,sizeof(cl_mem),&accel);     ERRCHECK
  error=clEnqueueNDRangeKernel(cq,trace_kern,2,NULL,gwsize,lwsize,1,&prepare,&trace);   ERRCHECK
#ifdef DEBUG
  gettimeofday(&starttime,NULL);
  error=clWaitForEvents(1,&trace);      ERRCHECK
  gettimeofday(&endtime,NULL);
  waitfor_trace+=timediff(starttime,endtime);
#endif
printf("tracing finished\n");

  nextcount=0;
  error=clEnqueueWriteBuffer(cq,ray_number,CL_TRUE,0,sizeof(cl_int),&nextcount,1,&trace,&extraevent);   ERRCHECK

  //shade raycount
  error=clSetKernelArg(shade_count_kern,0,sizeof(cl_mem),&hits);        ERRCHECK
  error=clSetKernelArg(shade_count_kern,1,sizeof(cl_int),&raycount);    ERRCHECK
  error=clSetKernelArg(shade_count_kern,2,sizeof(cl_mem),&ray_indexes); ERRCHECK
  error=clSetKernelArg(shade_count_kern,3,sizeof(cl_mem),&ray_number);  ERRCHECK
  error=clSetKernelArg(shade_count_kern,4,sizeof(cl_mem),&tris);        ERRCHECK
  error=clEnqueueNDRangeKernel(cq,shade_count_kern,2,NULL,gwsize,lwsize,1,&trace,&count);       ERRCHECK
#ifdef DEBUG
  gettimeofday(&starttime,NULL);
  error=clWaitForEvents(1,&count);      ERRCHECK
  gettimeofday(&endtime,NULL);
  waitfor_count+=timediff(starttime,endtime);
#endif
    //light trace
  error=clSetKernelArg(light_kern,0,sizeof(cl_mem),&hits);      ERRCHECK
  error=clSetKernelArg(light_kern,1,sizeof(cl_int),&raycount);  ERRCHECK
  error=clSetKernelArg(light_kern,2,sizeof(cl_mem),&light_samples);     ERRCHECK
  error=clSetKernelArg(light_kern,3,sizeof(cl_mem),&lights);    ERRCHECK
  error=clSetKernelArg(light_kern,4,sizeof(cl_int),&lightcount);        ERRCHECK
  error=clSetKernelArg(light_kern,5,sizeof(cl_mem),&tris);      ERRCHECK
  error=clSetKernelArg(light_kern,6,sizeof(cl_mem),&accel);     ERRCHECK
  error=clEnqueueNDRangeKernel(cq,light_kern,2,NULL,gwsize,lwsize,1,&trace,&light);     ERRCHECK
#ifdef DEBUG
  gettimeofday(&starttime,NULL);
  error=clWaitForEvents(1,&light);      ERRCHECK
  gettimeofday(&endtime,NULL);
  waitfor_light+=timediff(starttime,endtime);
#endif

  error=clEnqueueReadBuffer(cq,ray_number,CL_TRUE,0,sizeof(cl_int),&nextcount,1,&count,&extraevent);    ERRCHECK

  if ((nextcount>0)&&(nextcount>memcount))
  {
   cl_int inccount=MAX(2*memcount,nextcount);
   CLRELEASE(MemObject,rays);
   rays=clCreateBuffer(ctx, CL_MEM_READ_WRITE, sizeof(struct ray_s)*inccount,NULL,&error);      ERRCHECK
  }



    //shade
  error=clSetKernelArg(shade_kern,0,sizeof(cl_mem),&hits);      ERRCHECK
  error=clSetKernelArg(shade_kern,1,sizeof(cl_int),&raycount);  ERRCHECK
  error=clSetKernelArg(shade_kern,2,sizeof(cl_mem),&ray_indexes);       ERRCHECK
  error=clSetKernelArg(shade_kern,3,sizeof(cl_mem),&rays);      ERRCHECK
  error=clSetKernelArg(shade_kern,4,sizeof(cl_mem),&pixels);    ERRCHECK
  error=clSetKernelArg(shade_kern,5,sizeof(cl_mem),&light_samples);     ERRCHECK
  error=clSetKernelArg(shade_kern,6,sizeof(cl_mem),&lights);    ERRCHECK
  error=clSetKernelArg(shade_kern,7,sizeof(cl_int),&lightcount);        ERRCHECK
  error=clSetKernelArg(shade_kern,8,sizeof(cl_mem),&tris);      ERRCHECK
  error=clSetKernelArg(shade_kern,9,sizeof(cl_mem),&accel);     ERRCHECK

  for (int i=0;i<texnum;i++)
   error=clSetKernelArg(shade_kern,10+i,sizeof(cl_mem),&(hidden_textures[i]));  ERRCHECK

  error=clEnqueueNDRangeKernel(cq,shade_kern,2,NULL,gwsize,lwsize,1,&light,&shade);     ERRCHECK
#ifdef DEBUG
  gettimeofday(&starttime,NULL);
#endif
  error=clWaitForEvents(1,&shade);      ERRCHECK
#ifdef DEBUG
  gettimeofday(&endtime,NULL);
  waitfor_shade+=timediff(starttime,endtime);
#endif
  raycount = nextcount;
 }

 return 0;
cleanup:
 return error;
}








void opencl_cleanup()
{
 CLRELEASE(Event,prepare);
 CLRELEASE(Event,trace);
 CLRELEASE(Event,light);
 CLRELEASE(Event,shade);
 CLRELEASE(Event,count);
 CLRELEASE(Event,grid_prepare[0]);
 CLRELEASE(Event,grid_prepare[1]);
 CLRELEASE(Event,grid_prepare[2]);

 CLRELEASE(Event,scene_prepare[0]);
 CLRELEASE(Event,scene_prepare[1]);
 CLRELEASE(Event,scene_prepare[2]);
 CLRELEASE(Event,scene_prepare[3]);
 CLRELEASE(Event,scene_prepare[4]);
 CLRELEASE(Event,scene_prepare[5]);

 CLRELEASE(Event,extraevent);

 CLRELEASE(Kernel,trace_kern);
 CLRELEASE(Program,trace_prog);
 free (trace_prog_text);

 CLRELEASE(Kernel,light_kern);
 CLRELEASE(Program,light_prog);
 free (light_prog_text);

 CLRELEASE(Kernel,shade_kern);
 CLRELEASE(Kernel,shade_count_kern);
 CLRELEASE(Program,shade_prog);
 free (shade_prog_text);

 CLRELEASE(Kernel,tools_normals_kern);
 CLRELEASE(Kernel,tools_prirays_kern);
 CLRELEASE(Kernel,tools_lights_kern);
 CLRELEASE(Kernel,tools_copytex_kern);
 CLRELEASE(Program,tools_prog);
 free (tools_prog_text);

 CLRELEASE(Kernel,accel_kern);
 CLRELEASE(Program,accel_prog);
 free (accel_prog_text);

 CLRELEASE(MemObject,rays);
 CLRELEASE(MemObject,tris);
 CLRELEASE(MemObject,hits);
 CLRELEASE(MemObject,pixels);
 CLRELEASE(MemObject,accel);
 CLRELEASE(MemObject,ray_number);
 CLRELEASE(MemObject,ray_indexes);
 CLRELEASE(MemObject,lights);
 CLRELEASE(MemObject,light_samples);
 CLRELEASE(MemObject,seeds);

 if (accelstruct==ACCEL_GRID)
 {
  CLRELEASE(MemObject,hidden_buffers[0]);
  CLRELEASE(MemObject,hidden_buffers[1]);
  CLRELEASE(MemObject,hidden_buffers[2]);
  free (hidden_buffers);
 }

 for(int i=0;i<texnum;i++)
 {
  CLRELEASE(MemObject,hidden_textures[i]);
  free(textures[i].data);
  CLRELEASE(Event,push_texture[i]);
 }

 CLRELEASE(CommandQueue,cq);
 CLRELEASE(Context,ctx);

 free(image);
 free(push_texture);
 free(hidden_textures);
 free(randomseeds);

}
