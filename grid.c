/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * Copyright (C) 2010 Pavel Herrmann (morpheus.ibis@gmail.com)

 */

#include "grid.h"
#include <math.h>
#include <stdio.h>

#define MINX(tri) fmin(fmin(tri.spaceCoords[0].x,tri.spaceCoords[1].x),tri.spaceCoords[2].x)
#define MAXX(tri) fmax(fmax(tri.spaceCoords[0].x,tri.spaceCoords[1].x),tri.spaceCoords[2].x)
#define MINY(tri) fmin(fmin(tri.spaceCoords[0].y,tri.spaceCoords[1].y),tri.spaceCoords[2].y)
#define MAXY(tri) fmax(fmax(tri.spaceCoords[0].y,tri.spaceCoords[1].y),tri.spaceCoords[2].y)
#define MINZ(tri) fmin(fmin(tri.spaceCoords[0].z,tri.spaceCoords[1].z),tri.spaceCoords[2].z)
#define MAXZ(tri) fmax(fmax(tri.spaceCoords[0].z,tri.spaceCoords[1].z),tri.spaceCoords[2].z)
#define CROSS(dest,v1,v2) \
          dest[0]=v1[1]*v2[2]-v1[2]*v2[1]; \
          dest[1]=v1[2]*v2[0]-v1[0]*v2[2]; \
          dest[2]=v1[0]*v2[1]-v1[1]*v2[0]; 


int cell_triangle_intersect(struct triangle_s tri, float minx, float maxx, float miny, float maxy, float minz, float maxz)
{
 return 1;

 //plane-aabb intersection
 float normal[3];
 float fedge[3];
 float sedge[3];
 float tedge[3];
 fedge[0]=tri.spaceCoords[1].x-tri.spaceCoords[0].x;
 fedge[1]=tri.spaceCoords[1].y-tri.spaceCoords[0].y;
 fedge[2]=tri.spaceCoords[1].z-tri.spaceCoords[0].z;
 sedge[0]=tri.spaceCoords[2].x-tri.spaceCoords[0].x;
 sedge[1]=tri.spaceCoords[2].y-tri.spaceCoords[0].y;
 sedge[2]=tri.spaceCoords[2].z-tri.spaceCoords[0].z;
 tedge[0]=tri.spaceCoords[2].x-tri.spaceCoords[1].x;
 tedge[1]=tri.spaceCoords[2].y-tri.spaceCoords[1].y;
 tedge[2]=tri.spaceCoords[2].z-tri.spaceCoords[1].z;
 CROSS(normal,fedge,sedge);
 float minv=normal[0]*(normal[0]>0?minx:maxx)+normal[1]*(normal[1]>0?miny:maxy)+normal[2]*(normal[2]>0?minz:maxz);
 float maxv=normal[0]*(normal[0]>0?maxx:minx)+normal[1]*(normal[1]>0?maxy:miny)+normal[2]*(normal[2]>0?maxz:minz);
 float midv=normal[0]*tri.spaceCoords[0].x+normal[0]*tri.spaceCoords[0].y+normal[0]*tri.spaceCoords[0].z;
 if (!((minv<midv)&&(midv<maxv)))
  return 0;
 //more tests for precise triangle-aabb intersection
 return 1;
}



int create_grid(float targetoccupancy, int tricount, struct triangle_s* triangles, cl_int** griddata, cl_int** tridata, struct gridinfo_s* gridinfo)
{
 cl_int *lgriddata, *ltridata;
 gridinfo->minDimension.x=MINX(triangles[0]);
 gridinfo->minDimension.y=MINY(triangles[0]);
 gridinfo->minDimension.z=MINZ(triangles[0]);
 gridinfo->maxDimension.x=MAXX(triangles[0]);
 gridinfo->maxDimension.y=MAXY(triangles[0]);
 gridinfo->maxDimension.z=MAXZ(triangles[0]);

 for (size_t i=1;i<tricount;i++)
 {
  gridinfo->minDimension.x=fmin(MINX(triangles[i]),gridinfo->minDimension.x);
  gridinfo->minDimension.y=fmin(MINY(triangles[i]),gridinfo->minDimension.y);
  gridinfo->minDimension.z=fmin(MINZ(triangles[i]),gridinfo->minDimension.z);
  gridinfo->maxDimension.x=fmax(MAXX(triangles[i]),gridinfo->maxDimension.x);
  gridinfo->maxDimension.y=fmax(MAXY(triangles[i]),gridinfo->maxDimension.y);
  gridinfo->maxDimension.z=fmax(MAXZ(triangles[i]),gridinfo->maxDimension.z);
 }
 float xlength = gridinfo->maxDimension.x-gridinfo->minDimension.x;
 float ylength = gridinfo->maxDimension.y-gridinfo->minDimension.y;
 float zlength = gridinfo->maxDimension.z-gridinfo->minDimension.z;
 float norm = cbrt((tricount/targetoccupancy)/(xlength*ylength*zlength));
 gridinfo->cellCount.x=fmax(1,roundf(xlength*norm));
 gridinfo->cellCount.y=fmax(1,roundf(ylength*norm));
 gridinfo->cellCount.z=fmax(1,roundf(zlength*norm));
 gridinfo->cellSize.x=xlength/gridinfo->cellCount.x;
 gridinfo->cellSize.y=ylength/gridinfo->cellCount.y;
 gridinfo->cellSize.z=zlength/gridinfo->cellCount.z;

 size_t gridsize = gridinfo->cellCount.x * gridinfo->cellCount.y * gridinfo->cellCount.z;
 lgriddata = malloc(sizeof(cl_int)*(gridsize+1));
 for (size_t i=0;i<gridsize+1;i++)
  lgriddata[i]=0;
 int3 minidx;
 int3 maxidx;
 for (size_t i=0;i<tricount;i++)
 {
//count into griddata
  minidx.x=fmin(gridinfo->cellCount.x-1,(int)floor(((MINX(triangles[i])-gridinfo->minDimension.x)*gridinfo->cellCount.x)/xlength));
  maxidx.x=fmin(gridinfo->cellCount.x-1,(int)floor(((MAXX(triangles[i])-gridinfo->minDimension.x)*gridinfo->cellCount.x)/xlength));
  minidx.y=fmin(gridinfo->cellCount.y-1,(int)floor(((MINY(triangles[i])-gridinfo->minDimension.y)*gridinfo->cellCount.y)/ylength));
  maxidx.y=fmin(gridinfo->cellCount.y-1,(int)floor(((MAXY(triangles[i])-gridinfo->minDimension.y)*gridinfo->cellCount.y)/ylength));
  minidx.z=fmin(gridinfo->cellCount.z-1,(int)floor(((MINZ(triangles[i])-gridinfo->minDimension.z)*gridinfo->cellCount.z)/zlength));
  maxidx.z=fmin(gridinfo->cellCount.z-1,(int)floor(((MAXZ(triangles[i])-gridinfo->minDimension.z)*gridinfo->cellCount.z)/zlength));
  for (int x=minidx.x;x<=maxidx.x;x++)
   for (int y=minidx.y;y<=maxidx.y;y++)
    for (int z=minidx.z;z<=maxidx.z;z++)
    {
     if (cell_triangle_intersect(triangles[i],
	(gridinfo->minDimension.x+(x*gridinfo->cellSize.x)),(gridinfo->minDimension.x+((x+1)*gridinfo->cellSize.x)),
	(gridinfo->minDimension.y+(y*gridinfo->cellSize.y)),(gridinfo->minDimension.y+((y+1)*gridinfo->cellSize.y)),
	(gridinfo->minDimension.z+(z*gridinfo->cellSize.z)),(gridinfo->minDimension.z+((z+1)*gridinfo->cellSize.z)) ) == 1)
      (lgriddata[x + (gridinfo->cellCount.x * y) + (gridinfo->cellCount.x * gridinfo->cellCount.y * z)])++;
    }
  
 }
 for(size_t i=1; i<=gridsize; i++)
  lgriddata[i]+=lgriddata[i-1];
 ltridata = malloc(sizeof(cl_int)*lgriddata[gridsize]);
 for (size_t i=0;i<tricount;i++)
 {
//put indexes into tridata, decrease griddata
  minidx.x=fmin(gridinfo->cellCount.x-1,(int)floor(((MINX(triangles[i])-gridinfo->minDimension.x)*gridinfo->cellCount.x)/xlength));
  maxidx.x=fmin(gridinfo->cellCount.x-1,(int)floor(((MAXX(triangles[i])-gridinfo->minDimension.x)*gridinfo->cellCount.x)/xlength));
  minidx.y=fmin(gridinfo->cellCount.y-1,(int)floor(((MINY(triangles[i])-gridinfo->minDimension.y)*gridinfo->cellCount.y)/ylength));
  maxidx.y=fmin(gridinfo->cellCount.y-1,(int)floor(((MAXY(triangles[i])-gridinfo->minDimension.y)*gridinfo->cellCount.y)/ylength));
  minidx.z=fmin(gridinfo->cellCount.z-1,(int)floor(((MINZ(triangles[i])-gridinfo->minDimension.z)*gridinfo->cellCount.z)/zlength));
  maxidx.z=fmin(gridinfo->cellCount.z-1,(int)floor(((MAXZ(triangles[i])-gridinfo->minDimension.z)*gridinfo->cellCount.z)/zlength));
  for (int x=minidx.x;x<=maxidx.x;x++)
   for (int y=minidx.y;y<=maxidx.y;y++)
    for (int z=minidx.z;z<=maxidx.z;z++)
    {
     if (cell_triangle_intersect(triangles[i],
	(gridinfo->minDimension.x+(x*gridinfo->cellSize.x)),(gridinfo->minDimension.x+((x+1)*gridinfo->cellSize.x)),
	(gridinfo->minDimension.y+(y*gridinfo->cellSize.y)),(gridinfo->minDimension.y+((y+1)*gridinfo->cellSize.y)),
	(gridinfo->minDimension.z+(z*gridinfo->cellSize.z)),(gridinfo->minDimension.z+((z+1)*gridinfo->cellSize.z)) ) == 1)
      lgriddata[x + (gridinfo->cellCount.x*y) + (gridinfo->cellCount.x*gridinfo->cellCount.y * z)]--;
      ltridata[lgriddata[x + (gridinfo->cellCount.x*y) + (gridinfo->cellCount.x*gridinfo->cellCount.y * z)]]=i;
    }
 }
 *griddata=lgriddata;
 *tridata=ltridata;
 return 0;
}
