#ifndef _STDSHADER_H_
#define _STDSHADER_H_ 1

#include "types.h"

inline float3 flip(float3 direction, float3 normal)
{
 return -direction+( 2* dot(direction,normal) * normal );
}

inline float lambert(float3 normal, float3 rayDirection,
	__global const struct light_sample_s * light_samples, __global const struct light_s * lights, __const int lightcount)
{
 __private float ret=0.0f;
 for (int i=0;i<lightcount;i++)
 {
  if (lights[i].type==LIGHT_AMBIENT)
   ret+=max(0.0f,light_samples[i].intensity);
  else
   ret+=max(0.0f,light_samples[i].intensity*dot(-light_samples[i].direction,normal));
 }
 return ret;
}

inline float2 phong(float3 normal, float3 rayDirection, float exponent,
	__global const struct light_sample_s * light_samples, __global const struct light_s * lights, __const int lightcount)
{
 __private float2 ret=0.0f;
 for (int i=0;i<lightcount;i++)
 {
  if (lights[i].type==LIGHT_AMBIENT)
   ret.x+=max(0.0f,light_samples[i].intensity);
  else
   ret.x+=max(0.0f,light_samples[i].intensity*dot(-light_samples[i].direction,normal));	//diffuse term
   ret.y+=max(0.0f,light_samples[i].intensity*pow( dot( flip(-light_samples[i].direction,normal), rayDirection), exponent));	//specular term
 }
 return ret;
}


#endif	//_STDSHADER_H_
