/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * Copyright (C) 2010 Pavel Herrmann (morpheus.ibis@gmail.com)

 */

#include "shared.h"
#include "grid.h"
#include "ocl.h"
#include "conf.h"
#include "imageoutput.h"
#include "objloader.h"
#include "textures.h"
#include "interactive.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

cl_int tricount, mtlcount;
struct triangle_s * triangles=NULL;
char** mtllist=NULL;
char* mtllib=NULL;
cl_int* grid_data=NULL;
cl_int* cell_data=NULL;
struct gridinfo_s gridinfo;
struct mtl_def_s * materials=NULL;
struct texinfo_s * textures=NULL;
int gridcount, cellcount;
size_t imgsize[2];

extern struct pixel_s * image;
extern cl_mem pixels;
extern cl_event shade;
extern cl_event extraevent;
extern cl_command_queue cq;
extern cl_int pixcount;

int texnum=0;


#ifdef DEBUG
extern unsigned long waitfor_trace;
extern unsigned long waitfor_count;
extern unsigned long waitfor_light;
extern unsigned long waitfor_shade;
#endif

void cleanup(int status)
{
 opencl_cleanup();
 config_cleanup();

 for(int i=0;i<mtlcount;i++)
 {
  free(materials[i].texidx);
  free(mtllist[i]);
 }

 free (materials);
 free (textures);
 free (triangles);
 free (grid_data);
 free (cell_data);
 free (mtllist);

 exit (status);

}


int main(int argc, char** argv)
{
 if (argc!=2)
  return 1;


 if( config_load(argv[1]) )
  return 2;
//check target for %d in multiframe mode
 if (framecount>1)
 {
  char* substr = strstr(outname,"%d");
  if (substr==NULL)
   return 2;
  substr = strstr(substr+1,"%");
  if (substr!=NULL)
   return 2;
 }

 imgsize[0]=tilesize[0]*tiles.x;
 imgsize[1]=tilesize[1]*tiles.y;


 //load geometry, create grid
 loadOBJ(scenefile,&triangles,&tricount,&mtllist,&mtlcount,&mtllib);
 loadMTL(mtllib,mtlcount,mtllist,&materials);
 texnum=loadTextures(mtlcount,&textures,materials);

 printf("loaded %d triangles\n",tricount);
 if (accelstruct == ACCEL_GRID)
 {
  create_grid(density,tricount,triangles,&grid_data,&cell_data,&gridinfo);

  gridcount=(gridinfo.cellCount.x * gridinfo.cellCount.y * gridinfo.cellCount.z) +1;
  cellcount=grid_data[gridcount-1];
  printf("created %d cells, with %d entries\n",gridcount-1,cellcount);
  printf("grid resolution: %d %d %d\n",gridinfo.cellCount.x, gridinfo.cellCount.y, gridinfo.cellCount.z);
 }

 if (interactive)
  interactive_run(&argc,argv);
 else
 {
  error=opencl_prepare();	ERRCHECK
  error=prepare_buffers_kernels();	ERRCHECK

 //begin multiframe loop
  for (int f=0; f<framecount;f++)
  {
   current_frame=&(frames[f]);
//set ray count to original and realloc all ray-related buffers
printf("tracing frame %d\n",f);
   cl_int2 curtile;
   float * imgdata = malloc(sizeof(float)*(imgsize[0]*imgsize[1])*3);
   for (curtile.y=0; curtile.y<tiles.y; curtile.y++)
   {
    for (curtile.x=0; curtile.x<tiles.x; curtile.x++)
    {
     error=opencl_display(curtile);	ERRCHECK
     //retrieve the image
     error=clEnqueueReadBuffer(cq,pixels,CL_TRUE,0,sizeof(struct pixel_s)*pixcount,image,1,&shade,&extraevent);	ERRCHECK
     //convert the image

     for(int j=0;j<tilesize[1];j++)
      for(int i=0;i<tilesize[0];i++)
      {
       int innum = j*tilesize[0]+i;
       int outnum = (j+tilesize[1]*curtile.y)*imgsize[0] + i+tilesize[0]*curtile.x;

       imgdata[outnum*3]=image[innum].color.x;
       imgdata[outnum*3+1]=image[innum].color.y;
       imgdata[outnum*3+2]=image[innum].color.z;
      }

    }
   }
//write image
   char imgname[256];
   if (framecount>1)
    snprintf(imgname,255,outname,f);
   else
   {
    strncpy(imgname,outname,255);
    imgname[255]=0;
   }
   writeImage(imgname,imgsize[0],imgsize[1],imgdata);
   printf("output image written\n");
   free(imgdata);
  }
 }
 clFinish(cq);

//end multiframe loop
#ifdef DEBUG
 printf("elapsed times:\n trace %lu usec\n light %lu usec\n count %lu usec\n shade %lu usec\n", waitfor_trace, waitfor_light, waitfor_count, waitfor_shade);
#endif
 cleanup(0);
 cleanup:
 cleanup(1);
//release all resources

}

