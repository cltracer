
/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * Copyright (C) 2010 Pavel Herrmann (morpheus.ibis@gmail.com)

 */

#include <cstdlib>
#include <cstring>
#include <fstream>
#include <vector>
#include <string>


#define ISWHITE(x) ((x==' ')||(x=='\t'))

using namespace std;


extern "C"{
#include "types.h"
#include "objloader.h"

struct vertex
{
 float3 space;
 float3 normal;
 float2 texture;
};


 int addstring( vector<string>& v,string s)
 {
  int l=v.size();
  for (int i=0;i<l;i++)
   if (s.compare(v[i])==0)
    return i;
  v.push_back(s);
  return l;
 }




 int loadOBJ(char* filename, triangle_s ** out_tri, int * trinum, char*** mtllist, int * mtlnum, char** mtlfile)
 {
  vector<float3> vs;
  vector<float3> vns;
  vector<float2> vts;
  vector<triangle_s> tris;
  vector<string> mtls;
  ifstream file;
  file.open(filename,std::ios::in);
  string line;
  char* readpoint;
  int currentmtl=-1;
  int retval;
  
  while (file.good())
  {
   getline(file,line);
   if (line.length()==0)
    continue;
   readpoint=(char*) line.c_str();
   while (ISWHITE(readpoint[0]))
    readpoint++;
   switch (readpoint[0])
   {
    case 'v':
     switch(readpoint[1])
     {
      case '\t':
      case ' ':
       float3 v;
       retval=sscanf(readpoint+2,"%f %f %f",&(v.x),&(v.y),&(v.z));
       if (retval != 3)
        return -1;
       vs.push_back(v);
      break;
      case 'n':
       float3 vn;
       retval=sscanf(readpoint+3,"%f %f %f",&(vn.x),&(vn.y),&(vn.z));
       if (retval != 3)
        return -1;
       vns.push_back(vn);
      break;
      case 't':
       float2 vt;
       retval=sscanf(readpoint+3,"%f %f",&(vt.x),&(vt.y));
       if (retval != 2)
        return -1;
       vts.push_back(vt);
      break;
    }
    break;
    case 'f':
     int currvertex[3];
     vertex firstvtx,lastvtx,currvtx;
     triangle_s temptri;
     int t;
     int n;
     n=0;
     readpoint+=2;
     while (ISWHITE(readpoint[0]))
      readpoint++;
     while (readpoint[0]!=0)
     {
      for (int i=0;i<3;i++)
      {
       t=0;
       while((!ISWHITE(readpoint[0]))&&(readpoint[0]!='/'))
       {
        if (readpoint[0]==0)
         break;
        t=t*10+(readpoint[0]-48);
        readpoint++;
       }
       currvertex[i]=t;
       while((ISWHITE(readpoint[0]))||(readpoint[0]=='/'))
       {
        if (readpoint[0]==0)
         break;
        readpoint++;
       }
       if (t==0)
        goto nextline;
      }
      lastvtx=currvtx;
      currvtx.space=vs.at(currvertex[0]-1);
      currvtx.texture=vts.at(currvertex[1]-1);
      currvtx.normal=vns.at(currvertex[2]-1);
      if (n==0)
       firstvtx=currvtx;
      if (n>=2)
      {
       temptri.spaceCoords[0]=firstvtx.space;
       temptri.textureCoords[0]=firstvtx.texture;
       temptri.normalCoords[0]=firstvtx.normal;
       temptri.spaceCoords[1]=lastvtx.space;
       temptri.textureCoords[1]=lastvtx.texture;
       temptri.normalCoords[1]=lastvtx.normal;
       temptri.spaceCoords[2]=currvtx.space;
       temptri.textureCoords[2]=currvtx.texture;
       temptri.normalCoords[2]=currvtx.normal;
       temptri.objectIndex=currentmtl;
       tris.push_back(temptri);
      }
      n++;
     }
    break;
    case 'u':
     currentmtl=addstring(mtls,string(readpoint+7));
    break;
    case 'm':
     sscanf(readpoint+6, "%as", mtlfile);
    default:
    break;
   }
   nextline:;
  }
  *trinum = tris.size();
  *out_tri = (triangle_s*) malloc(sizeof(triangle_s)*(*trinum));
  copy(tris.begin(),tris.end(),*out_tri);
  *mtlnum = mtls.size();
  *mtllist = (char**) malloc(sizeof(char*)*(*mtlnum));
  for (int i=0;i<(*mtlnum);i++)
  {
   (*mtllist)[i] = (char*) malloc(sizeof(char)*(mtls.at(i).size()+1));
   strcpy((*mtllist)[i],mtls.at(i).c_str());
  }
 }


 int loadMTL (char* filename, int mtlnum, char** mtllist, struct mtl_def_s** out)
 {
  FILE * f = fopen(filename, "r");
  if (f==NULL)
   return 1;
  char * line;
  char * mtlname;
  char * textures;
  size_t length=0;
  int read;
  int texcount;
  int pos;
  int mtlidx;
  *out = (struct mtl_def_s*) malloc(sizeof(struct mtl_def_s)*mtlnum);
  while ( (read=getline(&line,&length,f))!=-1)
  {
   sscanf(line, "%as %d %n", &mtlname, &texcount, &pos);
   line+=pos;
   mtlidx=-1;
   for (int i=0;i<mtlnum;i++)
   {
    if (!strcmp(mtlname,mtllist[i]))
     mtlidx=i;
   }
   if (mtlidx==-1)
    continue;
   (*out)[mtlidx].texcount=texcount;
   (*out)[mtlidx].texnames=(char**) malloc(sizeof(char*)*texcount);
   for (int j=0;j<texcount;j++)
   {
    sscanf(line, "%as%n", &((*out)[mtlidx].texnames[j]), &pos);
    line+=pos;
   }
  }
 }



 
}


