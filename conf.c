/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * Copyright (C) 2010 Pavel Herrmann (morpheus.ibis@gmail.com)

 */

#include "types.h"
#include "shared.h"
#include "conf.h"
#include <stdio.h>
#include <string.h>

size_t tilesize[2];
size_t lwsize[2];
struct frameconfig_s * frames=NULL;
struct frameconfig_s * current_frame=NULL;
struct frameconfig_s last_frame;
int framecount;
cl_int lightcount;
cl_int2 tiles;
struct light_s * lightlist=NULL;
float density;
char * outname=NULL;
char * scenefile=NULL;
int accelstruct;
int interactive=0;

#define ISWHITE(x) ((x==' ')||(x=='\t'))
#define ISEOL(x) ((x=='0')||(x=='#')||(x=='\n'))
#define MAX(x,y) (x>y?x:y)


int config_load(char* filename)
{
 FILE * f = fopen(filename, "r");
 if (f==NULL)
  return 1;
 char * line=NULL;
 char * tline=NULL;
 size_t linelength=0;
 int read;
 while ( (read=getline(&line,&linelength,f))!=-1)
 {
  //don't want the newline char at the end of lines
  if(line[read-1]=='\n')
   line[read-1]=0;
  if(read<1)
   continue;
  tline=line;
  while(ISWHITE(tline[0]))
   tline++;
  if (ISEOL(tline[0]))
   continue;

  if(!strncmp(tline,"target",6))
  {
   tline+=7;
   while(ISWHITE(tline[0]))
    tline++;
   int len = strlen(tline);
   outname = malloc(sizeof(char)*(len+1));
   strncpy(outname,tline,len+1);
   continue;
  }

  if(!strncmp(tline,"objfile",7))
  {
   tline+=8;
   while(ISWHITE(tline[0]))
    tline++;
   int len = strlen(tline);
   scenefile = malloc(sizeof(char)*(len+1));
   strncpy(scenefile,tline,len+1);
   continue;
  }

  if(!strncmp(tline,"density",7))
  {
   tline+=8;
   while(ISWHITE(tline[0]))
    tline++;
   sscanf(tline,"%f",&density);
   continue;
  }

  if(!strncmp(tline,"tiles",5))
  {
   tline+=6;
   while(ISWHITE(tline[0]))
    tline++;
   sscanf(tline,"%d %d",&(tiles.x),&(tiles.y) );
   continue;
  }

  if(!strncmp(tline,"resolution",10))
  {
   tline+=11;
   while(ISWHITE(tline[0]))
    tline++;
   sscanf(tline,"%zd %zd",&(tilesize[0]),&(tilesize[1]) );
   continue;
  }

  if(!strncmp(tline,"worksize",8))
  {
   tline+=9;
   while(ISWHITE(tline[0]))
    tline++;
   sscanf(tline,"%zd %zd",&(lwsize[0]),&(lwsize[1]) );
   continue;
  }

  if(!strncmp(tline,"frames",6))
  {
   tline+=7;
   while(ISWHITE(tline[0]))
    tline++;
   if (!strncmp(tline,"interactive",11))
   {
    framecount = 1;
    interactive = 1;
   }
   else
   {
    sscanf(tline,"%d",&framecount );
    interactive = 0;
   }
    frames = malloc(sizeof(struct frameconfig_s)*framecount);
    current_frame = frames;
   continue;
  }

  if(!strncmp(tline,"frame",5))
  {
   int framenum;
   tline+=6;
   while(ISWHITE(tline[0]))
    tline++;
   sscanf(tline,"%d",&framenum );
   if (current_frame != NULL)
   {
    last_frame = *current_frame;
    current_frame = &(frames[framenum]);
    *current_frame = last_frame;
   }
   else
    current_frame = &(frames[framenum]);
   continue;
  }

  if(!strncmp(tline,"lookat",6))
  {
   tline+=7;
   while(ISWHITE(tline[0]))
    tline++;
   sscanf(tline,"%f %f %f",&(current_frame->lookat.x),&(current_frame->lookat.y),&(current_frame->lookat.z) );
   continue;
  }

  if(!strncmp(tline,"eyepoint",8))
  {
   tline+=9;
   while(ISWHITE(tline[0]))
    tline++;
   sscanf(tline,"%f %f %f",&(current_frame->eyepoint.x),&(current_frame->eyepoint.y),&(current_frame->eyepoint.z) );
   continue;
  }

  if(!strncmp(tline,"upvector",8))
  {
   tline+=9;
   while(ISWHITE(tline[0]))
    tline++;
   sscanf(tline,"%f %f %f",&(current_frame->upvector.x),&(current_frame->upvector.y),&(current_frame->upvector.z) );
   continue;
  }

  if(!strncmp(tline,"viewangle",9))
  {
   tline+=10;
   while(ISWHITE(tline[0]))
    tline++;
   sscanf(tline,"%f %f",&(current_frame->angles.x),&(current_frame->angles.y) );
   continue;
  }

  if(!strncmp(tline,"accelstruct",11))
  {
   tline+=12;
   while(ISWHITE(tline[0]))
    tline++;
   if (!strncmp(tline,"grid",4))
   {
    accelstruct=ACCEL_GRID;
    continue;
   }
   if (!strncmp(tline,"dummy",5))
   {
    accelstruct=ACCEL_DUMMY;
    continue;
   }
   return 1;
  }

  if(!strncmp(tline,"lights",6))
  {
   tline+=7;
   while(ISWHITE(tline[0]))
    tline++;
   sscanf(tline,"%d",&lightcount );
   lightlist = malloc(sizeof(struct light_s)*lightcount);
   continue;
  }

  if(!strncmp(tline,"light",5))
  {
   int lightnum;
   int charcount=0;
   struct light_s * currlight=NULL;
   tline+=6;
   while(ISWHITE(tline[0]))
    tline++;
   sscanf(tline,"%d %n",&lightnum,&charcount );
   currlight=&(lightlist[lightnum]);
   tline+=charcount;
   while(ISWHITE(tline[0]))
    tline++;
   if (!strncmp(tline,"ambient",7))
   {
    currlight->type=LIGHT_AMBIENT;
    tline+=8;
   }
   else if (!strncmp(tline,"omni",4))
   {
    currlight->type=LIGHT_OMNI;
    tline+=5;
   }
   else if (!strncmp(tline,"directional",11))
   {
    currlight->type=LIGHT_DIRECTIONAL;
    tline+=12;
   }
   else if (!strncmp(tline,"spot",4))
   {
    currlight->type=LIGHT_SPOT;
    tline+=5;
   }
   while(ISWHITE(tline[0]))
    tline++;
   sscanf(tline,"%f [%f %f %f] %n", &currlight->intensity, &currlight->color.x, &currlight->color.y, &currlight->color.z,&charcount);
   tline+=charcount;
   while(ISWHITE(tline[0]))
    tline++;
   switch (currlight->type)
   {
    case LIGHT_AMBIENT:
     break;
    case LIGHT_OMNI:
     sscanf(tline,"[%f %f %f]", &currlight->omni.position.x, &currlight->omni.position.y, &currlight->omni.position.z);
     break;
    case LIGHT_DIRECTIONAL:
     sscanf(tline,"[%f %f %f]", &currlight->directional.direction.x, &currlight->directional.direction.y, &currlight->directional.direction.z);
     break;
    case LIGHT_SPOT:
     sscanf(tline,"[%f %f %f] [%f %f %f] %f %f", &currlight->omni.position.x, &currlight->omni.position.y, &currlight->omni.position.z,
        &currlight->spot.direction.x, &currlight->spot.direction.y, &currlight->spot.direction.z, &currlight->spot.coneangle, &currlight->spot.cutoff);
     break;
    default:
     return 1;
   }
   continue;
  }

 }

 free(line);
 return 0;
}

void config_cleanup(void)
{
 free(outname);
 free(scenefile);
 free(lightlist);
 free(frames);
}
