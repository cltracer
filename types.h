/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * Copyright (C) 2010 Pavel Herrmann (morpheus.ibis@gmail.com)

 */


#ifndef _TYPES_H_
#define _TYPES_H_ 1

#ifndef __OPENCL_VERSION__
#include <CL/cl.h>
#include <CL/cl_platform.h>
typedef cl_float3 float3;
typedef cl_float2 float2;
typedef cl_int3 int3;
typedef cl_int2 int2;

#endif


struct triangle_s
{
 float3 spaceCoords[3];
 float3 normalCoords[3];
 float2 textureCoords[3];
 int objectIndex;
};

typedef struct triangle_s triangle_t;

struct pixel_s
{
 float3 color;
 float fill;
 unsigned int lock;
};


struct reconstruct_s
{
 int pixelNumber;
 float importance;
 unsigned int randomSeed;
};


struct ray_s
{
 float3 origin;
 float3 direction;
 struct reconstruct_s reconstructInfo;
};

typedef struct ray_s ray_t;

struct hit_s
{
 float3 baryCoords;
 float3 rayDirection;
 int triIndex;
 struct reconstruct_s reconstructInfo;
};

typedef struct hit_s hit_t;

struct dummyaccel_s
{
#ifdef __OPENCL_VERSION__
 int triangleCount;
#else
 cl_int triangleCount;
#endif
};

typedef struct dummyaccel_s dummyaccel_t;

struct gridinfo_s
{
 float3 minDimension;
 float3 maxDimension;
 float3 cellSize;
 int3 cellCount;
};

typedef struct gridinfo_s gridinfo_t;

struct gridaccel_s
{
#ifdef __OPENCL_VERSION__
 __global const struct gridinfo_s * gridinfo;
 __global const int * grid;
 __global const int * cells;
#else
 cl_mem gridinfo;
 cl_mem grid;
 cl_mem cells;
#endif
};

typedef struct gridaccel_s gridaccel_t;

struct accel_s
{
 int type;
 union
 {
  struct dummyaccel_s dummy;
  struct gridaccel_s grid;
 };
};

#define ACCEL_DUMMY 0
#define ACCEL_GRID 1

typedef struct accel_s accel_t;


struct texinfo_s
{
 unsigned char * data;
 int3 dimensions;
 int bpp;
};

typedef struct texinfo_s texinfo_t;

struct mtl_def_s
{
 int texcount;
 char** texnames;
 int* texidx;
};

typedef struct mtl_def_s mtl_def_t;

struct omni_light_s
{
 float3 position;
};

struct directional_light_s
{
 float3 direction;
};

struct spot_light_s
{
 float3 position;
 float3 direction;
 float coneangle;
 float cutoff;
};


struct light_s
{
 float3 color;
 float intensity;
 int type;
 union
 {
  struct omni_light_s omni;
  struct directional_light_s directional;
  struct spot_light_s spot;
 };
};

#define LIGHT_AMBIENT 0
#define LIGHT_OMNI 1
#define LIGHT_DIRECTIONAL 2
#define LIGHT_SPOT 3

typedef struct light_s light_t;

struct light_sample_s
{
 float3 color;
 float intensity;
 float3 direction;
};

typedef struct light_sample_s light_sample_t;

#endif //_TYPES_H_
