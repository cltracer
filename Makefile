CC=gcc
CFLAGS=-g -O2 -std=gnu99
CXX=g++
CXXFLAGS=-g -O2 -std=gnu++98
LDFLAGS=-lm -lIL -lOpenCL -lglut
OBJECTS=main.o grid.o textures.o imageoutput.o ocl.o conf.o objloader.o interactive.o

all: tracer

tracer: ${OBJECTS}
	${CXX} ${LDFLAGS} -o $@ ${OBJECTS}

clean:
	rm -f ${OBJECTS} tracer

