/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * Copyright (C) 2010 Pavel Herrmann (morpheus.ibis@gmail.com)

 */

#ifndef _CONF_H_
#define _CONF_H_

#include <CL/cl.h>

struct frameconfig_s
{
 cl_float3 eyepoint;
 cl_float3 lookat;
 cl_float3 upvector;
 cl_float2 angles;
};

extern size_t tilesize[2];
extern size_t lwsize[2];
extern struct frameconfig_s * frames;
extern struct frameconfig_s * current_frame;
extern struct frameconfig_s last_frame;
extern int framecount;
extern cl_int lightcount;
extern cl_int2 tiles;
extern struct light_s * lightlist;
extern float density;
extern char * outname;
extern char * scenefile;
extern int accelstruct;
extern int interactive;

int config_load(char*);
void config_cleanup(void);


#endif
